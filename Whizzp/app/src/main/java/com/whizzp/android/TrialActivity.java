package com.whizzp.android;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

import com.whizzp.android.Adapters.InstructionsAdapter;
import com.whizzp.android.Model.Instruction;
import com.whizzp.android.Views.slidingimages.CirclePageIndicator;

public class TrialActivity extends AppCompatActivity {

    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private ArrayList<Instruction> instructionsArrayList;

    private int[] myInstructionList = new int[]{com.whizzp.android.R.drawable.university_instructions, com.whizzp.android.R.drawable.swipe_right_instruction, com.whizzp.android.R.drawable.swipe_left_instruction,
            com.whizzp.android.R.drawable.archive_instruction, com.whizzp.android.R.drawable.rate_instruction
            , com.whizzp.android.R.drawable.archive};

    private String[] myInstructionListText = new String[]{"Get connected with your university!","Swipe right to like!", "Swipe left to dislike!","If you want to decide later, ARCHIVE!","You can rate people!",""};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.whizzp.android.R.layout.trial_main_screen);

        instructionsArrayList = new ArrayList<>();
        instructionsArrayList = populateList();

          init();

    }

    private ArrayList<Instruction> populateList(){

        ArrayList<Instruction> list = new ArrayList<>();

        for(int i = 0; i < 6; i++){
            Instruction mInstruction = new Instruction();
            mInstruction.setImageDrawable(myInstructionList[i]);
            mInstruction.setInstruction_text(myInstructionListText[i]);
            list.add(mInstruction);
        }

        return list;
    }

    private void init() {

        mPager = (ViewPager) findViewById(com.whizzp.android.R.id.main_slider_pager);
        mPager.setAdapter(new InstructionsAdapter(TrialActivity.this, instructionsArrayList));

        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(com.whizzp.android.R.id.main_slider);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

        //Set circle indicator radius
        indicator.setRadius(5 * density);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

}