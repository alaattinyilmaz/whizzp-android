package com.whizzp.android.Model;

public class Preferences {

	// TODO: Server model will be changed as below!

	private Integer id;
	private User user;
	private Short maximumDistance;
	private Short preferredGender;
	private Short minimumAge;
	private Short maximumAge;
	private boolean discoverable;
	private boolean universityFilter;
	//private boolean rankingFilter;
	private Short minimumRanking;
	private Short maximumRanking;


	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Short getMaximumDistance() {
		return maximumDistance;
	}

	public void setMaximumDistance(Short maximumDistance) {
		this.maximumDistance = maximumDistance;
	}

	public Short getPreferredGender() {
		return preferredGender;
	}

	public void setPreferredGender(Short preferredGender) {
		this.preferredGender = preferredGender;
	}

	public Short getMinimumAge() {
		return minimumAge;
	}

	public void setMinimumAge(Short minimumAge) {
		this.minimumAge = minimumAge;
	}

	public Short getMaximumAge() {
		return maximumAge;
	}

	public void setMaximumAge(Short maximumAge) {
		this.maximumAge = maximumAge;
	}

	public Boolean getDiscoverable() {
		return discoverable;
	}

	public void setDiscoverable(Boolean discoverable) {
		this.discoverable = discoverable;
	}

	public Boolean getUniversityFilter() {
		return universityFilter;
	}

	public void setUniversityFilter(Boolean universityFilter) {
		this.universityFilter = universityFilter;
	}

	/*public Boolean getRankingFilter() {
		return rankingFilter;
	}

	public void setRankingFilter(Boolean rankingFilter) {
		this.rankingFilter = rankingFilter;
	}*/

	public Short getMinimumRanking() {
		return minimumRanking;
	}

	public void setMinimumRanking(Short minimumRanking) {
		this.minimumRanking = minimumRanking;
	}

	public Short getMaximumRanking() {
		return maximumRanking;
	}

	public void setMaximumRanking(Short maximumRanking) {
		this.maximumRanking = maximumRanking;
	}
}
