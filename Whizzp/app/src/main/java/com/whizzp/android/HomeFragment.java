package com.whizzp.android;

import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import com.whizzp.android.Model.User;
import com.whizzp.android.Views.placeholderview.SwipeDecor;
import com.whizzp.android.Views.placeholderview.SwipePlaceHolderView;

public class HomeFragment extends Fragment {

    public static SwipePlaceHolderView mSwipeView;
    private Button load_more_button;
    public static List<ProfileCard> profileCards;
    public static List<User> mUsers;
    public static boolean isCreatedBefore = false;
   // public static CustomProgressDialog progressDialog;
    View view;
    public static HomeFragment self;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(com.whizzp.android.R.layout.fragment_home, container, false);

        mSwipeView = (SwipePlaceHolderView) view.findViewById(com.whizzp.android.R.id.swipeView);

     //   load_more_button = (Button) view.findViewById(R.id.load_more_button);

        Point windowSize = SwipePlaceHolderView.getDisplaySize(getActivity().getWindowManager());
        int bottomMargin = SwipePlaceHolderView.dpToPx(130);

        mSwipeView.getBuilder()
                .setDisplayViewCount(2) // How many cards will be shown on the screen
                .setHeightSwipeDistFactor(10)
                .setWidthSwipeDistFactor(5)
                .setSwipeDecor(new SwipeDecor()
                        .setViewWidth(windowSize.x)
                        .setViewHeight(windowSize.y - bottomMargin)
                        .setViewGravity(Gravity.TOP)
                        .setPaddingTop(5) // How much show on bottom
                        .setRelativeScale(0.01f)
                        .setSwipeInMsgLayoutId(com.whizzp.android.R.layout.liked_cardview_msg)
                        .setSwipeArchieveMsgLayoutId(com.whizzp.android.R.layout.archieved_card_view_msg)
                        .setSwipeOutMsgLayoutId(com.whizzp.android.R.layout.disliked_card_view_msg));

        getCardList();


        /*
        load_more_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCardList();
            }
        });
        */

        return view;
    }


    public void getCardList()
    {

        if(!isCreatedBefore) {
            if (NetworkHandler.self != null) {
                profileCards = new ArrayList<ProfileCard>();
                mUsers = new ArrayList<User>();
                NetworkHandler.self.setHomeFirstCreation(true);
                    if(NetworkHandler.self.isAuthorized())
                    {
                        NetworkHandler.self.getSwipeableUsers();
                    }
                }
            isCreatedBefore = true;
        }


        else if(profileCards.size() == 0)
        {
            if (NetworkHandler.self != null) {
                NetworkHandler.self.getSwipeableUsers();
            }
        }

        else if(profileCards.size() > 0)
        {

            for (User user : mUsers)
            {
                mSwipeView.addView(new ProfileCard(getContext(), user, mSwipeView, HomeFragment.this));
            }

        }


    }


}
