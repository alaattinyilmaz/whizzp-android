package com.whizzp.android.Constants;

public class InteractionType {
	public static final Short DISLIKE = 0;
	public static final Short LIKE = 1;
	public static final Short MATCH = 2;
	public static final Short ARCHIVE = 3;
	public static final Short BLOCKED = 4;
	public static final Short BLOCKEDFRIENDS = 5;
}
