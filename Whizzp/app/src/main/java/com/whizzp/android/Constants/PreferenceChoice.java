package com.whizzp.android.Constants;

public class PreferenceChoice {
	
	public static final Boolean CHECKED = true;
	public static final Boolean UNCHECKED = false;
	public static final Short DISTANCE_MAX = 100;//in km
	public static final Short AGE_MIN = 18;
	public static final Short AGE_MAX = 99;
	public static final Short RANKING_MIN = 1;
	public static final Short RANKING_MAX = 5;

}
