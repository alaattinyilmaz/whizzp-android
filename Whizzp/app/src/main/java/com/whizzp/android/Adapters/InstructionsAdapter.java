package com.whizzp.android.Adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import com.whizzp.android.Model.Instruction;
import com.whizzp.android.R;

public class InstructionsAdapter extends PagerAdapter {

    private ArrayList<Instruction> instructionsArrayList;
    private LayoutInflater inflater;
    private Context context;

    public InstructionsAdapter(Context context, ArrayList<Instruction> instructionsArrayList) {
        this.context = context;
        this.instructionsArrayList = instructionsArrayList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return instructionsArrayList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.adapter_instructions, view, false);

        assert imageLayout != null;

        ImageView sliding_image = (ImageView) imageLayout.findViewById(R.id.instructions_sliding_images);

        TextView instruction_text_view = (TextView) imageLayout.findViewById(R.id.instruction_text_view);

        instruction_text_view.setText(instructionsArrayList.get(position).getInstruction_text());
        sliding_image.setImageResource(instructionsArrayList.get(position).getImageDrawable());


        // Glide.with(context).load(ImageArrayList.get(position).getImageURL()).into(sliding_image);

        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}