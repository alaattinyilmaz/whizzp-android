package com.whizzp.android.Constants;

public class Gender {
	public static final Short MALE = 0;
	public static final Short FEMALE = 1;
	public static final Short MALEANDFEMALE = 2; 
}
