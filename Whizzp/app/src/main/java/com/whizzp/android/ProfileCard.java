package com.whizzp.android;


import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.whizzp.android.Constants.InteractionType;
import com.whizzp.android.Model.User;
import com.whizzp.android.Views.placeholderview.SwipePlaceHolderView;
import com.whizzp.android.Views.placeholderview.annotations.Layout;
import com.whizzp.android.Views.placeholderview.annotations.Resolve;
import com.whizzp.android.Views.placeholderview.annotations.swipe.SwipeArchieveState;
import com.whizzp.android.Views.placeholderview.annotations.swipe.SwipeCancelState;
import com.whizzp.android.Views.placeholderview.annotations.swipe.SwipeInState;
import com.whizzp.android.Views.placeholderview.annotations.swipe.SwipeOut;
import com.whizzp.android.Views.placeholderview.annotations.swipe.SwipeOutState;
import com.whizzp.android.Constants.Personal;
import com.whizzp.android.Views.placeholderview.annotations.View;
import com.whizzp.android.Views.placeholderview.annotations.swipe.SwipeArchieve;
import com.whizzp.android.Views.placeholderview.annotations.swipe.SwipeIn;

@Layout(com.whizzp.android.R.layout.profile_cardview)
public class ProfileCard {

    @View(com.whizzp.android.R.id.profileImageView)
    private ImageView profileImageView;

    @View(com.whizzp.android.R.id.like_button_view)
    private ImageView like_button_view;

    @View(com.whizzp.android.R.id.dislike_button_view)
    private ImageView dislike_button_view;

    @View(com.whizzp.android.R.id.archive_button_view)
    private ImageView archive_button_view;

    @View(com.whizzp.android.R.id.nameAgeTxt)
    private TextView nameAgeTxt;

    @View(com.whizzp.android.R.id.university_name_view)
    private TextView university_name_view;

    private User mProfile;
    private Context mContext;
    private SwipePlaceHolderView mSwipeView;
    private HomeFragment homeFragment;
    private FragmentManager fragmentManager;
    private UserProfileFragment userProfileFragment;

    public static final int SWIPE_DISLIKE = 0;
    public static final int SWIPE_LIKE = 1;
    public static final int SWIPE_ARCHIEVE = 2;



    public ProfileCard(Context context, User profile, SwipePlaceHolderView swipeView, HomeFragment homeFragment) {
        mContext = context;
        mProfile = profile;
        mSwipeView = swipeView;
        this.homeFragment = homeFragment;
        fragmentManager = Personal.FRAGMENT_MANAGER;
    }



    @Resolve
    private void onResolved() {


       Glide.with(mContext).load(mProfile.getProfilePicture()).into(profileImageView);
        nameAgeTxt.setText(mProfile.getFirstName() + ", " + mProfile.getLastName());
        university_name_view.setText(mProfile.getLastName()); // TODO: It will be university name


        profileImageView.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {

                if(userProfileFragment == null) {
                    userProfileFragment = new UserProfileFragment();
                }

                // Passing user details to created fragment
                userProfileFragment.passUser(mProfile);

                // Begining transaction
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(com.whizzp.android.R.id.main_container, userProfileFragment).addToBackStack("UserProfile").commit();


            }
        });

        like_button_view.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                mSwipeView.doSwipe(SWIPE_LIKE);
            }
        });


        archive_button_view.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
               mSwipeView.doSwipe(SWIPE_ARCHIEVE);
            }
        });

        dislike_button_view.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                mSwipeView.doSwipe(SWIPE_DISLIKE);
            }
        });

    }


    @SwipeOut
    private void onSwipedOut() {
        swipeAction();
        if(NetworkHandler.self != null){
            NetworkHandler.self.addInteraction(mProfile.getId(), InteractionType.DISLIKE);
        }
    }

    @SwipeCancelState
    private void onSwipeCancelState() {
        Log.d("EVENT", "onSwipeCancelState");
    }

    @SwipeIn
    private void onSwipeIn() {
        swipeAction();
        if(NetworkHandler.self != null){
            NetworkHandler.self.addInteraction(mProfile.getId(), InteractionType.LIKE);
        }

    }

    @SwipeArchieve
    private void onSwipeArchieve() {
        NetworkHandler.self.addInteraction(mProfile.getId(), InteractionType.ARCHIVE);
        swipeAction();
    }


    @SwipeInState
    private void onSwipeInState() {

    }

    @SwipeOutState
    private void onSwipeOutState() {


    }

    @SwipeArchieveState
    private void onSwipeArchieveState() {

    }


    // Since a profile is swipped iff it is the first element in the list, so delete the 0th element.
    private void swipeAction() {
        this.homeFragment.profileCards.remove(0);
        this.homeFragment.mUsers.remove(0);
    }
}