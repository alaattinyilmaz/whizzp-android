package com.whizzp.android;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;

import java.util.ArrayList;
import java.util.List;

import com.whizzp.android.Adapters.LikeAdapter;
import com.whizzp.android.Constants.InteractionType;
import com.whizzp.android.Constants.Personal;
import com.whizzp.android.Model.User;
import com.whizzp.android.Views.progressdialog.CustomProgressDialog;
import com.whizzp.android.Views.pulltorefresh.PullToRefreshView;
import com.whizzp.android.Model.Interaction;

public class LikeFragment extends Fragment {

    private TextView like_text_view;
    private boolean isCreatedBefore = false;
    private boolean dialogChecker = true;
    private View view;

    private List<User> myUsers;
    private PullToRefreshView mPullToRefreshView;
    private ListView interaction_list_view;
    private LikeAdapter mInteractionAdapter;
   // private ProgressDialog dialog;
    private CustomProgressDialog progressDialog;
    private ImageView interaction_type_view;
    private FragmentManager fragmentManager;
    private UserProfileFragment userProfileFragment;
    private FragmentTransaction fragmentTransaction;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        // This statement is called once at the first creation of this fragment for memory allocations
        if (!isCreatedBefore) {
            myUsers = new ArrayList();
            //  fragmentManager = getFragmentManager();

            fragmentManager = Personal.FRAGMENT_MANAGER;

            mInteractionAdapter = new LikeAdapter(getContext(), myUsers);

            AndroidNetworking.initialize(getActivity().getApplicationContext());

            // fragmentManager = getFragmentManager();
            userProfileFragment = new UserProfileFragment();

           // dialog = new ProgressDialog(getActivity());
            progressDialog = new CustomProgressDialog(getContext());
            progressDialog.show();

            // Getting JSON data from link
            getInteractionData();
            isCreatedBefore = true;
        }


        View view = inflater.inflate(com.whizzp.android.R.layout.fragment_like, container, false);

        interaction_list_view = (ListView) view.findViewById(com.whizzp.android.R.id.interaction_list);

        //Setting adapter
        interaction_list_view.setAdapter(mInteractionAdapter);


        interaction_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // Passing user details to created fragment
                userProfileFragment.passUser(myUsers.get(position));

                    // Begining transaction
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(com.whizzp.android.R.id.main_container, userProfileFragment).addToBackStack("UserProfile1").commit();
                    return;

            }
        });



        // Pull to refresh view
        mPullToRefreshView = (PullToRefreshView) view.findViewById(com.whizzp.android.R.id.pull_to_refresh);

        mPullToRefreshView.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPullToRefreshView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getInteractionData();
                        mPullToRefreshView.setRefreshing(false);
                    }
                }, 1000); // REFRESH_DELAY
            }
        });

        return view;

    }

    private void getInteractionData() {
        if (NetworkHandler.self != null) {
            ParsedRequestListener<List<Interaction>> parsedRequestListener = new ParsedRequestListener<List<Interaction>>() {
                @Override
                public void onResponse(List<Interaction> response) {
                    myUsers.clear();
                    System.out.println("this is response");
                    System.out.println(response.size());

                    // TODO: They will be deleted
                    if (response.size() == 0) {
                        User user = new User();
                        user.setFirstName("KIMSE YOK AMK");
                        myUsers.add(user);
                    } else {
                        for (Interaction interaction : response) {
                            User user = new User();
                      //      System.out.println(interaction.getTo().getFirstName());
                            user.setFirstName(interaction.getTo().getFirstName());
                            user.setProfilePicture(interaction.getTo().getProfilePicture());

                            if(interaction.getType() == InteractionType.LIKE)
                            {
                                myUsers.add(user);
                            }

                        }

                    }

                    mInteractionAdapter.notifyDataSetChanged();

                    if (dialogChecker) {
                        progressDialog.dismiss();
                        dialogChecker = false;
                    }
                }

                @Override
                public void onError(ANError anError) {
                    System.out.print(anError.getErrorBody());
                    System.out.println(anError.getErrorCode());
                    System.out.println(anError.getMessage());
                    anError.printStackTrace();
                }
            };
            NetworkHandler.self.getAllLikedFromUser(parsedRequestListener);

        }


    }


}
