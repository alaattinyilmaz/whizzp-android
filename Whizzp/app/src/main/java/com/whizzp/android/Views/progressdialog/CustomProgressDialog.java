package com.whizzp.android.Views.progressdialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.whizzp.android.R;

public class CustomProgressDialog extends Dialog {
    private ImageView imageview;

    public CustomProgressDialog(Context context) {
        super(context, R.style.TransparentProgressDialog);
        WindowManager.LayoutParams windowmanger = getWindow().getAttributes();
        windowmanger.gravity = Gravity.CENTER_HORIZONTAL;
        getWindow().setAttributes(windowmanger );
        setTitle(null);
       // setCancelable(false);
       // setOnCancelListener(null);
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams params = new  LinearLayout.LayoutParams(250, 250);
        imageview = new ImageView(context);
        imageview.setBackgroundResource(R.drawable.progress_animation);
        layout.addView(imageview, params);
        addContentView(layout, params);
    }

    @Override
    public void show() {
        super.show();
        AnimationDrawable frameAnimation = (AnimationDrawable)imageview.getBackground();
        frameAnimation.start();

    }

}