package com.whizzp.android.Adapters;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import com.whizzp.android.Model.User;
import com.whizzp.android.R;
import com.whizzp.android.UserProfileFragment;
import com.whizzp.android.Views.circulartransform.CircleTransform;

public class LikeAdapter extends ArrayAdapter<User> {

    public LikeAdapter(Context context, List<User> items) {
        super(context, 0, items);
    }

    private User item;
    private UserProfileFragment userProfileFragment;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        item = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_like_interaction, parent, false);
        }


        ImageView like_profile_picture_view = (ImageView) convertView.findViewById(R.id.like_profile_picture_view);

        Glide.with(getContext()).load(item.getProfilePicture()).transform(new CircleTransform(getContext())).into(like_profile_picture_view);

        TextView person_name = (TextView) convertView.findViewById(R.id.like_username_view);
        //  TextView date = (TextView) convertView.findViewById(R.id.mydate);

        //Toast.makeText(getContext(), item.getFirstName(), Toast.LENGTH_SHORT).show();
        person_name.setText(item.getFirstName());


        return convertView;
    }

}
