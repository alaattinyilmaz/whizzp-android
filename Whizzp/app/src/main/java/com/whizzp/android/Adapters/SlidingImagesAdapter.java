package com.whizzp.android.Adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import com.whizzp.android.Model.Image;
import com.whizzp.android.R;

public class SlidingImagesAdapter extends PagerAdapter {

    private ArrayList<Image> ImageArrayList;
    private LayoutInflater inflater;
    private Context context;

    public SlidingImagesAdapter(Context context, ArrayList<Image> ImageArrayList) {
        this.context = context;
        this.ImageArrayList = ImageArrayList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return ImageArrayList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.adapter_sliding_images, view, false);

        assert imageLayout != null;

        ImageView sliding_image = (ImageView) imageLayout.findViewById(R.id.sliding_image);

        Glide.with(context).load(ImageArrayList.get(position).getImageURL()).into(sliding_image);

        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}