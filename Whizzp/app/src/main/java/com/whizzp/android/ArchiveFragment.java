package com.whizzp.android;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;

import java.util.ArrayList;
import java.util.List;

import com.whizzp.android.Adapters.ArchieveAdapter;
import com.whizzp.android.Constants.InteractionType;
import com.whizzp.android.Constants.Personal;
import com.whizzp.android.Model.User;
import com.whizzp.android.Views.progressdialog.CustomProgressDialog;
import com.whizzp.android.Views.pulltorefresh.PullToRefreshView;
import com.whizzp.android.Model.Interaction;

public class ArchiveFragment extends Fragment {

    private TextView like_text_view;
    private boolean isCreatedBefore = false;
    private boolean dialogChecker = true;
    private View view;

    private List<User> myUsers;
    private PullToRefreshView mPullToRefreshView;
    private ListView archieve_list_view;;
    private ArchieveAdapter mArchieveAdapter;;
    private CustomProgressDialog progressDialog;
    private FragmentManager fragmentManager;
    private UserProfileFragment userProfileFragment;
    private FragmentTransaction fragmentTransaction;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {



        // This statement is called once at the first creation of this fragment page
        if (!isCreatedBefore) {
            myUsers = new ArrayList();
            // fragmentManager = getFragmentManager();
            fragmentManager = Personal.FRAGMENT_MANAGER;
            mArchieveAdapter = new ArchieveAdapter(getContext(), myUsers);
            AndroidNetworking.initialize(getActivity().getApplicationContext());
            userProfileFragment = new UserProfileFragment();
            // TODO : Progress dialog will be deleted or customized
            progressDialog = new CustomProgressDialog(getActivity());
            progressDialog.show();

            // Getting JSON data from link
            getInteractionData();
            isCreatedBefore = true;
        }




        View view = inflater.inflate(com.whizzp.android.R.layout.fragment_archive, container, false);

        archieve_list_view = (ListView) view.findViewById(com.whizzp.android.R.id.archieve_list);

        archieve_list_view.setAdapter(mArchieveAdapter);


        // Getting profile
        archieve_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                userProfileFragment.passUser(myUsers.get(position));

                if(!userProfileFragment.isAdded())
                {
                    // Begining transaction
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(com.whizzp.android.R.id.main_container, userProfileFragment).addToBackStack("UserProfile2").commit();
                    return;
                }


            }
        });



        // Swipe to refresh view
        mPullToRefreshView = (PullToRefreshView) view.findViewById(com.whizzp.android.R.id.archieve_pull_to_refresh);

        mPullToRefreshView.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPullToRefreshView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getInteractionData();
                        mPullToRefreshView.setRefreshing(false);
                    }
                }, 1000); // REFRESH_DELAY
            }
        });

        return view;

    }

    private void getInteractionData() {
        if (NetworkHandler.self != null) {
            ParsedRequestListener<List<Interaction>> parsedRequestListener = new ParsedRequestListener<List<Interaction>>() {
                @Override
                public void onResponse(List<Interaction> response) {
                    myUsers.clear();
                   // System.out.println("this is response");
                    // System.out.println(response.size());


                    if (response.size() == 0) {
                        User user = new User();
                        user.setFirstName("KIMSE YOK AMK");
                        myUsers.add(user);
                    } else {
                        for (Interaction interaction : response) {
                            User user = new User();
                          //  System.out.println(interaction.getTo().getFirstName());
                            user.setFirstName(interaction.getTo().getFirstName());
                            user.setProfilePicture(interaction.getTo().getProfilePicture());
                            if(interaction.getType() == InteractionType.ARCHIVE)
                            {
                                myUsers.add(user);
                            }

                        }

                    }

                    mArchieveAdapter.notifyDataSetChanged();

                    if (dialogChecker) {
                        progressDialog.dismiss();
                        dialogChecker = false;
                    }
                }

                @Override
                public void onError(ANError anError) {
                    System.out.print(anError.getErrorBody());
                    System.out.println(anError.getErrorCode());
                    System.out.println(anError.getMessage());
                    anError.printStackTrace();
                }
            };
            NetworkHandler.self.getAllArchivedFromUser(parsedRequestListener);

        }


    }

}
