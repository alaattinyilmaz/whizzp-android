package com.whizzp.android.Model;


//@Entity
public class Interaction {
	//@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	//@ManyToOne
	//@JoinColumn(name = "from_user_id")
	private User from;

	//@ManyToOne
	//@JoinColumn(name = "to_user_id")
	private User to;

	private Short type;

	//@CreationTimestamp
	//@Temporal(TemporalType.TIMESTAMP)
	private Long date;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public User getFrom() {
		return from;
	}

	public void setFrom(User from) {
		this.from = from;
	}

	public User getTo() {
		return to;
	}

	public void setTo(User to) {
		this.to = to;
	}

	public Short getType() {
		return type;
	}

	public void setType(Short type) {
		this.type = type;
	}

	public Long getDate() {
		return date;
	}

	public void setDate(Long date) {
		this.date = date;
	}

}
