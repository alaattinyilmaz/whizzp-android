package com.whizzp.android;

import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.whizzp.android.Model.User;
import com.whizzp.android.Views.placeholderview.SwipeDecor;
import com.whizzp.android.Views.placeholderview.SwipePlaceHolderView;
import com.whizzp.android.Views.progressdialog.CustomProgressDialog;

import java.util.ArrayList;
import java.util.List;

public class RankingFragment extends Fragment {

    SwipePlaceHolderView mSwipeView;
    List<RateCard> rateCards;
    List<User> mUsers;
    CustomProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(com.whizzp.android.R.layout.fragment_ranking, container, false);

        mSwipeView = (SwipePlaceHolderView) view.findViewById(com.whizzp.android.R.id.rateView);

        Point windowSize = SwipePlaceHolderView.getDisplaySize(getActivity().getWindowManager());
        int bottomMargin = SwipePlaceHolderView.dpToPx(130);

        mSwipeView.getBuilder()
                .setDisplayViewCount(2) // How many cards will be shown on the screen
                .setHeightSwipeDistFactor(10)
                .setWidthSwipeDistFactor(5)
                .setSwipeDecor(new SwipeDecor()
                        .setViewWidth(windowSize.x)
                        .setViewHeight(windowSize.y - bottomMargin)
                        .setViewGravity(Gravity.TOP)
                        .setPaddingTop(5) // How much show on bottom
                        .setRelativeScale(0.01f)
                        .setSwipeInMsgLayoutId(com.whizzp.android.R.layout.high_ranked_card_view_msg)
                        .setSwipeArchieveMsgLayoutId(com.whizzp.android.R.layout.average_ranked_card_view_msg)
                        .setSwipeOutMsgLayoutId(com.whizzp.android.R.layout.low_ranked_card_view_msg));

        mSwipeView.disableTouchSwipe();


        if (rateCards == null) {
            progressDialog = new CustomProgressDialog(getContext());
            //profiles = Utils.loadProfiles(getContext());
            if (NetworkHandler.self != null) {

                progressDialog.show();
                rateCards = new ArrayList<RateCard>();
                mUsers = new ArrayList<User>();

                ParsedRequestListener<List<User>> parsedRequestListener = new ParsedRequestListener<List<User>>() {
                    @Override
                    public void onResponse(List<User> response) {
                        // System.out.println(response);
                        for (User user : response) {
                            mUsers.add(user);
                            rateCards.add(new RateCard(getContext(), user, mSwipeView, RankingFragment.this));
                        }
                        for (RateCard rateCard : rateCards) {
                            mSwipeView.addView(rateCard);
                        }

                        progressDialog.dismiss();
                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                    }
                };
                NetworkHandler.self.getRateableUsers(parsedRequestListener);
            }
        }
        else if (rateCards.size() == 0)
        {

        }

        else {

            for (User user : mUsers)
            {
                mSwipeView.addView(new RateCard(RankingFragment.this.getContext(), user, mSwipeView, RankingFragment.this));
            }


        }


        return view;
    }
}
