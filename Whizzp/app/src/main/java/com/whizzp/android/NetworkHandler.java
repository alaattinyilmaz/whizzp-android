package com.whizzp.android;

import android.content.SharedPreferences;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.whizzp.android.Constants.Personal;
import com.whizzp.android.Model.Image;
import com.whizzp.android.Model.Interaction;
import com.whizzp.android.Model.Preferences;
import com.whizzp.android.Model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by kerimgokarslan on 06/07/2017.
 */

public class NetworkHandler {
    public static final String API_ADDRESS = "http://35.187.176.16:8080/api/";
    public static final String AUTH_ADDRESS = "http://35.187.176.16:8080/oauth/token";
    public static final String REGISTER_ADDRESS = "http://35.187.176.16:8090/";
    public static NetworkHandler self;


    private String username;
    private String facebookToken;
    private String accessToken;
    private String refreshToken;
    private boolean isAuthorized;

    private boolean isFirstCreation = false;

    public boolean isFirstCreation() {
        return isFirstCreation;
    }

    AndroidNetworking androidNetworking;


    public ParsedRequestListener<List<User>> swipeableUsersList;

    public boolean isAuthorized() {
        return isAuthorized;
    }

    /**
     * @return
     */
    public void isAuthorized(OkHttpResponseListener okHttpResponseListener) {
        AndroidNetworking.post(API_ADDRESS + "is_authorized")
                .addHeaders("Authorization", "Bearer " + accessToken)
                .setPriority(Priority.IMMEDIATE)
                .build().getAsOkHttpResponse(okHttpResponseListener);


    }

    NetworkHandler(SharedPreferences sharedPreferences) {
        if ((accessToken = sharedPreferences.getString("access_token", "")).length() == 0) {
            refreshToken = "";
            isAuthorized = false;

        } else {
            refreshToken = sharedPreferences.getString("refresh_token", "");
            isAuthorized = true;
            // also a network check can be done here iff accesstoken is expired or not!

        }

    }


    public void authorize(final String mUsername, final String mFacebookToken, final SharedPreferences sharedPreferences, final MainActivity mainActivity) {

        System.out.println("I'll try to auth!");
        AndroidNetworking.post(AUTH_ADDRESS)
                .addHeaders("Authorization", "Basic d2hpenpwOjEyMzQ1Ng==")
                .addBodyParameter("grant_type", "password")
                .addBodyParameter("scope", "write")
                .addBodyParameter("client_secret", "123456")
                .addBodyParameter("client_id", "whizzp")
                .addBodyParameter("username", mUsername)
                .addBodyParameter("password", mFacebookToken)
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //  System.out.println(response);
                        try {
                            accessToken = (String) response.get("access_token");
                            refreshToken = (String) response.get("refresh_token");
                            facebookToken = mFacebookToken;
                            username = mUsername;
                            isAuthorized = true;

                            Toast.makeText(Personal.CONTEXT_MAIN, "You are now authorized!", Toast.LENGTH_SHORT).show();


                            //TODO: In case of authorization failed after first creation this can cause no loading of cards, reconsider it in the future!
                            if (isFirstCreation) {
                                getSwipeableUsers();
                            }


                            //System.out.println(response.get("token_type"));
                            //System.out.println(response.get("expires_in"));
                            //System.out.println(response.get("scope"));
                        } catch (JSONException e) {
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("access_token", "");
                            editor.putString("refresh_token", "");
                            editor.commit();
                            isAuthorized = false;
                            // TODO error and stay at login

                            Toast.makeText(Personal.CONTEXT_MAIN, "You DO NOT HAVE PERMISSION!", Toast.LENGTH_SHORT).show();


                            e.printStackTrace();
                            return;
                        }
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        System.out.println(accessToken);
                        System.out.println("im commintg!!");
                        editor.putString("access_token", accessToken);
                        editor.putString("refresh_token", refreshToken);
                        editor.commit();
                        isAuthorized = true;
                        mainActivity.onCreateLoader();


                    }

                    @Override
                    public void onError(ANError anError) {
                        System.out.println("error");
                        System.err.println(anError.getErrorBody());

                        System.out.print("Hata veriyorum.");

                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("access_token", "");
                        editor.putString("refresh_token", "");
                        editor.commit();
                        // TODO error and stay at login

                    }
                });

    }

    public void addInteraction(Integer toId, Short type) {
        if (isAuthorized) {
            AndroidNetworking.post(API_ADDRESS + "add_interaction")
                    .addHeaders("Authorization", "Bearer " + accessToken)
                    .addBodyParameter("to_id", "" + toId)
                    .addBodyParameter("type", "" + type)
                    .setPriority(Priority.IMMEDIATE)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });
        }

    }


    public void getUserPreferences(ParsedRequestListener parsedRequestListener) {
        if (isAuthorized) {
            AndroidNetworking.post(API_ADDRESS + "get_preferences_of_user")
                    .addHeaders("Authorization", "Bearer " + accessToken)
                    .setPriority(Priority.IMMEDIATE)
                    .build()
                    .getAsObject(Preferences.class, parsedRequestListener);
        } else {
            System.out.println("ARE YOU AUTH?");
        }
    }


    public void getAllInteractionsFromUser(/*OkHttpResponseListener ok*/ParsedRequestListener<List<Interaction>> parsedRequestListener) {
        if (isAuthorized) {
            AndroidNetworking.post(API_ADDRESS + "get_all_interactions_from_user")
                    .addHeaders("Authorization", "Bearer " + accessToken)
                    .setPriority(Priority.IMMEDIATE)
                    .build()
                    //.getAsOkHttpResponse(ok);
                    .getAsObjectList(Interaction.class, parsedRequestListener);
        } else {
            System.out.println("ARE YOU AUTH?");
        }
    }

    public void getAllArchivedFromUser(/*OkHttpResponseListener ok*/ParsedRequestListener<List<Interaction>> parsedRequestListener) {
        if (isAuthorized) {
            AndroidNetworking.post(API_ADDRESS + "get_all_archived_from_user")
                    .addHeaders("Authorization", "Bearer " + accessToken)
                    .setPriority(Priority.IMMEDIATE)
                    .build()
                    //.getAsOkHttpResponse(ok);
                    .getAsObjectList(Interaction.class, parsedRequestListener);
        } else {
            System.out.println("ARE YOU AUTH?");
        }
    }

    public void getAllLikedFromUser(/*OkHttpResponseListener ok*/ParsedRequestListener<List<Interaction>> parsedRequestListener) {
        if (isAuthorized) {
            AndroidNetworking.post(API_ADDRESS + "get_all_liked_from_user")
                    .addHeaders("Authorization", "Bearer " + accessToken)
                    .setPriority(Priority.IMMEDIATE)
                    .build()
                    //.getAsOkHttpResponse(ok);
                    .getAsObjectList(Interaction.class, parsedRequestListener);
        } else {
            System.out.println("ARE YOU AUTH?");
        }
    }

    public void rateUser(final String user, final String rate, OkHttpResponseListener requestListener) {
        if (isAuthorized) {
            AndroidNetworking.post(API_ADDRESS + "rate_user")
                    .addHeaders("Authorization", "Bearer " + accessToken)
                    .addBodyParameter("user", user)
                    .addBodyParameter("rate", rate)
                    .setPriority(Priority.IMMEDIATE)
                    .build()
                    //.getAsOkHttpResponse(ok);
                    .getAsOkHttpResponse(requestListener);
            //.getAsObjectList(Interaction.class, parsedRequestListener);
        } else {
            System.out.println("ARE YOU AUTH?");
        }
    }


    public void setHomeFirstCreation(boolean isFirstCreation) {
        this.isFirstCreation = isFirstCreation;
    }


    public void getSwipeableUsers() {

        if (isAuthorized) {


            AndroidNetworking.post(API_ADDRESS + "get_swipeable_users")
                    .addHeaders("Authorization", "Bearer " + accessToken)
                    .setTag(this)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsObjectList(User.class, new ParsedRequestListener<List<User>>() {
                        @Override
                        public void onResponse(List<User> users) {

                            for (User user : users) {
                                HomeFragment.self.mUsers.add(user);
                                HomeFragment.self.profileCards.add(new ProfileCard(Personal.CONTEXT_MAIN, user, HomeFragment.self.mSwipeView, HomeFragment.self));
                            }

                            for (ProfileCard profileCard : HomeFragment.self.profileCards) {
                                HomeFragment.self.mSwipeView.addView(profileCard);
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            anError.printStackTrace();
                            System.out.println("This is error document!");
                            System.out.print(isAuthorized);
                            Toast.makeText(Personal.CONTEXT_MAIN, "ERROR OCCURED!", Toast.LENGTH_SHORT).show();

                        }
                    });
        }

    }



    public void getRateableUsers(ParsedRequestListener<List<User>> parsedRequestListener) {
        if (isAuthorized) {
            AndroidNetworking.post(API_ADDRESS + "get_rateable_users")
                    .addHeaders("Authorization", "Bearer " + accessToken)
                    .setPriority(Priority.IMMEDIATE)
                    .build()
                    //.getAsOkHttpResponse(ok);
                    .getAsObjectList(User.class, parsedRequestListener);
        } else {
            System.out.println("ARE YOU AUTH?");
        }
    }



    // TODO: This request will be changed as IMAGE URL receiver
    public void getUserProfileImages(ParsedRequestListener<List<Image>> parsedRequestListener) {
        if (isAuthorized) {
            AndroidNetworking.post(API_ADDRESS + "get_images_of_user")
                    .addHeaders("Authorization", "Bearer " + accessToken)
                    .setPriority(Priority.IMMEDIATE)
                    .build()
                    //.getAsOkHttpResponse(ok);
                    .getAsObjectList(Image.class, parsedRequestListener);
        } else {
            System.out.println("ARE YOU AUTH?");
        }
    }

    public void getMyself(ParsedRequestListener parsedRequestListener) {
        if (isAuthorized) {
            AndroidNetworking.post(API_ADDRESS + "get_myself")
                    .addHeaders("Authorization", "Bearer " + accessToken)
                    .setPriority(Priority.IMMEDIATE)
                    .build()
                    //.getAsOkHttpResponse(ok);
                    .getAsObject(User.class, parsedRequestListener);
        } else {
            System.out.println("ARE YOU AUTH?");
        }
    }

    /**
     * TODO INCOMPLETE
     * @param preferences
     * @param okHttpResponseListener
     */
    public void savePreferencesOfUser(Preferences preferences, OkHttpResponseListener okHttpResponseListener) {

       /* public @ResponseBody String savePreferencesOfUSer(Principal principal, @RequestParam boolean discoverable,
        @RequestParam Short maximumAge, @RequestParam Short maximumDistance, @RequestParam Short minimumAge,
        @RequestParam Short minimumRanking, @RequestParam Short preferredGender,
        @RequestParam Boolean universityFilter) {*/

        // TODO ADD ALL PARAMS  ABOVE
        AndroidNetworking.post(API_ADDRESS + "save_preferences_of_user")
                .addHeaders("Authorization", "Bearer " + accessToken)
                .addBodyParameter("maximumDistance", "" + preferences.getMaximumDistance())
                .addBodyParameter("discoverable", "" + preferences.getDiscoverable())
                .addBodyParameter("minimumAge", "" + preferences.getMinimumAge())
                .addBodyParameter("maximumAge", "" + preferences.getMaximumAge())
                .addBodyParameter("minimumRanking", "" + preferences.getMinimumRanking())
                .addBodyParameter("maximumRanking", "" + preferences.getMaximumRanking())
                .addBodyParameter("preferredGender", "" + preferences.getPreferredGender())
                .addBodyParameter("universityFilter", "" + preferences.getUniversityFilter())
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getAsOkHttpResponse(okHttpResponseListener);


    }

    public void logout(SharedPreferences sharedPreferences) {
        //also logout from network by post req.
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("access_token", "");
        editor.putString("refresh_token", "");
        editor.commit();
        isAuthorized = false;

    }


}
