package com.whizzp.android.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import com.whizzp.android.Model.User;
import com.whizzp.android.Views.circulartransform.CircleTransform;
import com.whizzp.android.R;

public class ArchieveAdapter extends ArrayAdapter<User> {


    private User item;

    public ArchieveAdapter(Context context, List<User> items) {
        super(context, 0, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        item = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_archieve, parent, false);
        }

        ImageView archieve_profile_picture_view = (ImageView) convertView.findViewById(R.id.archieve_profile_picture_view);

        Glide.with(getContext()).load(item.getProfilePicture()).transform(new CircleTransform(getContext())).into(archieve_profile_picture_view);


        TextView person_name = (TextView) convertView.findViewById(R.id.archieve_username_view);
        //  TextView date = (TextView) convertView.findViewById(R.id.mydate);

        person_name.setText(item.getFirstName());
        // date.setText(item.getLastname());
        return convertView;
    }


}
