package com.whizzp.android;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;

import java.util.ArrayList;
import java.util.List;

import com.whizzp.android.Adapters.SlidingImagesAdapter;
import com.whizzp.android.Model.Image;
import com.whizzp.android.Model.User;
import com.whizzp.android.Views.slidingimages.CirclePageIndicator;


public class UserProfileFragment extends Fragment {


    private TextView username_view;
    private TextView user_university_name_view;
    private TextView back_text;
    private User user;
    private ImageView profile_image_view;
    private float density;
    private CirclePageIndicator indicator;
    private ViewPager mPager;
    private static int currentPage = 0;
    private ArrayList<Image> ImageArrayList;
    private boolean isCreatedBefore = false;
    private SlidingImagesAdapter mSlidingImagesAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(com.whizzp.android.R.layout.fragment_user_profile, container, false);


        loadImages();

        // Creating pager
        mPager = (ViewPager) view.findViewById(com.whizzp.android.R.id.image_slide_pager);

        mSlidingImagesAdapter = new SlidingImagesAdapter(getActivity(), ImageArrayList);

        mPager.setAdapter(mSlidingImagesAdapter);

        // Creating indicator to slide images
        indicator = (CirclePageIndicator) view.findViewById(com.whizzp.android.R.id.image_slide_indicator_view);
        indicator.setViewPager(mPager);
        density = getResources().getDisplayMetrics().density;

        //Set circle indicator radius
        indicator.setRadius(5 * density);

        //profile_image_view = (ImageView)view.findViewById(R.id.user_profile_image_view);

        // Glide.with(this).load(user.getProfilePicture()).into(profile_image_view); // Setting profile image from URL


        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

        // TODO pls don't use _ use instead usernameView..
        // Setting profile values
        username_view = (TextView) view.findViewById(com.whizzp.android.R.id.person_name_and_age_view);
        username_view.setText(user.getFirstName());

        user_university_name_view = (TextView) view.findViewById(com.whizzp.android.R.id.user_university_name_view);
        user_university_name_view.setText(user.getUsername()); // TODO: It will be university name


        // TODO: they might be class vars, AND NAMING CONVENTIONS PLS FOR IDS!
        TextView rankingView = (TextView) view.findViewById(com.whizzp.android.R.id.user_rank_value);
        rankingView.setText("" + user.getRanking() / 10000.0);

        TextView likeView = (TextView) view.findViewById(com.whizzp.android.R.id.user_like_value);
        likeView.setText(user.getLikePercent() + "%/" + (100 - user.getLikePercent()) + "%");

        TextView universityView = (TextView) view.findViewById(com.whizzp.android.R.id.user_university_name_view);
        // TODO this one is doesn't work since university classes don't have any instances on server
        universityView.setText("Sabanci University");
        //universityView.setText(user.getUniversity().getName());



        // TODO: In the GUI there will be a BLOCK USER icon!!

        back_text = (TextView) view.findViewById(com.whizzp.android.R.id.back_text);

        back_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        return view;

    }

    // Getting user from each ProfileCard
    public void passUser(User user) {
        this.user = user;
    }


    public void loadImages() {

        if (ImageArrayList == null) {
            ImageArrayList = new ArrayList<>();
        }
        // EXPERIMENTAL - TODO: It will take imageURL array of an user

        if (NetworkHandler.self != null) {
            ParsedRequestListener<List<Image>> parsedRequestListener = new ParsedRequestListener<List<Image>>() {
                @Override
                public void onResponse(List<Image> response) {
                    // System.out.println(response);

                    // TODO: Need to change it is loading only first creation on clicking images in like and archive pages profiles
                    // TODO: If it is loaded before it will not change UI -> It can be reconsiderable maybe user deleted and loaded a different image
                    if (ImageArrayList.size() != response.size() || !isCreatedBefore) {

                        for (Image image : response) {


                            ImageArrayList.add(image);
                            mSlidingImagesAdapter.notifyDataSetChanged();

                        }
                    }

                    if (!isCreatedBefore) {
                        isCreatedBefore = true;
                    }


                }

                @Override
                public void onError(ANError anError) {
                    anError.printStackTrace();
                }
            };
            NetworkHandler.self.getUserProfileImages(parsedRequestListener);
        }

    }


}
