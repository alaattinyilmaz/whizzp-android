package com.whizzp.android;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.whizzp.android.Constants.PreferenceChoice;
import com.whizzp.android.Model.Preferences;
import com.whizzp.android.Views.rangeseekbar.RangeSeekBar;

import okhttp3.Response;


public class SettingsFragment extends Fragment {


    private SeekBar max_km_seek_bar;
    private RangeSeekBar age_range_seekbar;
    private RangeSeekBar ranking_bar_seekbar;

    private TextView max_km_text_view;
    private int max_km_choice;

    private TextView age_range_value_view;
    private TextView ranking_range_value_view;

    private Spinner gender_choice_spinner;

    private Button apply_changes_button;

    private Short minAgeValue;
    private Short maxAgeValue;

    private Short minRankingValue;
    private Short maxRankingValue;

    private SwitchCompat visible_switch;

    private SwitchCompat university_switch;


    private int ID;
    private int userID;
    private Short maximumDistance;
    private Short preferredGender;
    private Short minimumAge;
    private Short maximumAge;
    private boolean discoverable;
    private boolean universityFilter;
    private Short minimumRanking;
    private Short maximumRanking = 5;


    private Short preferredGenderValue;
    private boolean universityFilterValue;
    private boolean discoverableValue;

    private boolean isCreatedBefore = false;

    private Preferences userPreferences;
    private Preferences changedPreferences;

    SharedPreferences sharedPreferences;

    Handler viewHandler;
    Runnable viewRunnable;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(com.whizzp.android.R.layout.fragment_settings, container, false);

        max_km_seek_bar = (SeekBar) view.findViewById(com.whizzp.android.R.id.max_km_seekbar);
        max_km_text_view = (TextView) view.findViewById(com.whizzp.android.R.id.max_km_text_view);
        max_km_seek_bar.setMax(PreferenceChoice.DISTANCE_MAX);

        visible_switch = (SwitchCompat) view.findViewById(com.whizzp.android.R.id.visible_switch);

        university_switch = (SwitchCompat) view.findViewById(com.whizzp.android.R.id.university_switch);

        age_range_seekbar = (RangeSeekBar) view.findViewById(com.whizzp.android.R.id.age_range_seekbar);
        age_range_value_view = (TextView) view.findViewById(com.whizzp.android.R.id.age_range_value_view);
        age_range_seekbar.setRangeValues(PreferenceChoice.AGE_MIN, PreferenceChoice.AGE_MAX);
        age_range_seekbar.setNotifyWhileDragging(true);

        ranking_range_value_view = (TextView) view.findViewById(com.whizzp.android.R.id.ranking_range_value_view);
        ranking_bar_seekbar = (RangeSeekBar) view.findViewById(com.whizzp.android.R.id.ranking_range_seekbar);
        ranking_bar_seekbar.setRangeValues(PreferenceChoice.RANKING_MIN, PreferenceChoice.RANKING_MAX);
        ranking_bar_seekbar.setNotifyWhileDragging(true);

        age_range_seekbar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Number minValue, Number maxValue) {
                minimumAge = minValue.shortValue();
                maximumAge = maxValue.shortValue();

                if (minAgeValue != maxAgeValue) {
                    age_range_value_view.setText(String.valueOf(minimumAge) + "-" + String.valueOf(maximumAge));
                } else {
                    age_range_value_view.setText(String.valueOf(minimumAge));
                }

            }

        });


        gender_choice_spinner = (Spinner) view.findViewById(com.whizzp.android.R.id.gender_choice_spinner);


        // TODO: This condition can be changed into sharedPreferences.getInt... FIRST_LOGIN if after clicked logout
        if (!isCreatedBefore) {
            getPreferences();
        } else {
            setPreferencesValues();
            changeView();
        }


        ranking_bar_seekbar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Number minValue, Number maxValue) {

                minimumRanking = minValue.shortValue();
                maximumRanking = maxValue.shortValue();

                if (minValue == maxValue) {
                    ranking_range_value_view.setText(String.valueOf(minValue));
                } else {
                    ranking_range_value_view.setText(String.valueOf(minValue) + "-" + String.valueOf(maxValue));
                }

            }

        });


        max_km_seek_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                max_km_text_view.setText(String.valueOf(progress) + " km");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                maximumDistance = (short) seekBar.getProgress();
            }
        });

        university_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                universityFilterValue = isChecked;
            }
        });


        visible_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                discoverableValue = isChecked;
            }
        });


        gender_choice_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                preferredGenderValue = (short) position;
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        apply_changes_button = (Button) view.findViewById(com.whizzp.android.R.id.apply_changes_button);


        apply_changes_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyPreferencesChanges();
            }
        });

        //TODO: Edit Ghost List will be implemented

        //TODO: Edit Friends of Friend List will be implemented

        //TODO: Logout page will be implemented

        //TODO: Delete Your Account will be implemented

        return view;
    }

    public void changeView() {
        // Maximum Distance

        max_km_seek_bar.setProgress(0); // call these two methods before setting progress.
        max_km_seek_bar.setProgress(maximumDistance);


        max_km_text_view.setText(String.valueOf(max_km_seek_bar.getProgress()) + " km");


        // Setting age bar values
        age_range_seekbar.setSelectedMinValue(minimumAge);
        age_range_seekbar.setSelectedMaxValue(maximumAge);

        minAgeValue = age_range_seekbar.getSelectedMinValue().shortValue();
        maxAgeValue = age_range_seekbar.getSelectedMaxValue().shortValue();
        age_range_value_view.setText(String.valueOf(minimumAge) + "-" + String.valueOf(maximumAge));

        // Discoverable switch and university filter checks
        //  visible_switch.setChecked(discoverable);
        //   university_switch.setChecked(universityFilter);


        max_km_seek_bar.setProgress(maximumDistance);

        age_range_seekbar.setSelectedMinValue(minimumAge);
        age_range_seekbar.setSelectedMaxValue(maximumAge);

        university_switch.setChecked(universityFilter);

        visible_switch.setChecked(discoverable);
        System.out.println(minimumRanking);
        System.out.println(maximumRanking);
        maximumRanking = 5; // TODO coz beni
        ranking_bar_seekbar.setSelectedMinValue(minimumRanking);
        ranking_bar_seekbar.setSelectedMaxValue(maximumRanking);

        gender_choice_spinner.setSelection(preferredGender);


        viewHandler = new Handler();
        viewRunnable = new Runnable() {
            public void run() {


                gender_choice_spinner.setSelection(preferredGender);

                max_km_seek_bar.setProgress(maximumDistance);

                age_range_seekbar.setSelectedMinValue(minimumAge);
                age_range_seekbar.setSelectedMaxValue(maximumAge);

                university_switch.setChecked(universityFilter);

                visible_switch.setChecked(discoverable);

                ranking_bar_seekbar.setSelectedMinValue(minimumRanking);
                ranking_bar_seekbar.setSelectedMaxValue(maximumRanking);
            }
        };


        viewHandler.postDelayed(viewRunnable, 10);


        // Ranking bar customization
        ranking_bar_seekbar.setSelectedMinValue(minimumRanking);
        ranking_bar_seekbar.setSelectedMaxValue(maximumRanking);
        ranking_range_value_view.setText(String.valueOf(minimumRanking) + "-" + String.valueOf(maximumRanking));


    }


    /*public Preferences getTrialPreferences()
    {
        userPreferences = new Preferences();

        userPreferences.setId(1);
        userPreferences.setUserI1);
        userPreferences.setMaximumDistance(13);
        userPreferences.setMinimumAge(21);
        userPreferences.setMaximumAge(30);
        userPreferences.setMinimumRanking(3);
        userPreferences.setMaximumRanking(5);
        userPreferences.setDiscoverable(true);
        userPreferences.setRankingFilter(false);
        userPreferences.setUniversityFilter(false);
        userPreferences.setPreferredGender(Gender.FEMALE);
        userPreferences.getDiscoverable();

        return userPreferences;

    }*/




    public void getPreferences() {

        if (NetworkHandler.self != null) {

            ParsedRequestListener<Preferences> parsedRequestListener = new ParsedRequestListener<Preferences>() {
                @Override
                public void onResponse(Preferences response) {


                    userPreferences = response;

                    setPreferencesValues(); // Assigning preferences values into local variables
                    // commitPreferences(); // Saving preferences values to sharedpreferences
                    changeView();

                    isCreatedBefore = true;

                }

                @Override
                public void onError(ANError anError) {
                    anError.printStackTrace();
                }
            };
            NetworkHandler.self.getUserPreferences(parsedRequestListener);
        }

    }


    public void changePreferences() {
        changedPreferences = new Preferences();
        changedPreferences = userPreferences;
        changedPreferences.setMaximumDistance(maximumDistance);
        changedPreferences.setMinimumAge(minimumAge);
        changedPreferences.setMaximumAge(maximumAge);
        changedPreferences.setMinimumRanking(minimumRanking);
        changedPreferences.setMaximumRanking(maximumRanking);
        changedPreferences.setDiscoverable(discoverableValue);
        //userPreferences.setRankingFilter(rankingFilter);
        changedPreferences.setUniversityFilter(universityFilterValue);
        changedPreferences.setPreferredGender(preferredGenderValue);
        // return changedPreferences;
    }


    public void applyPreferencesChanges() {

        changePreferences();

        // TODO first update the values in "userPreferences" variable, after updating call
        if (NetworkHandler.self != null) {
            OkHttpResponseListener okHttpResponseListener = new OkHttpResponseListener() {
                @Override
                public void onResponse(Response response) {
                    System.out.println(response.code());

                    Toast.makeText(getContext(), "Your changes applied successfully!", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(ANError anError) {
                    anError.printStackTrace();
                    // TODO raise error

                }
            };
            NetworkHandler.self.savePreferencesOfUser(changedPreferences, okHttpResponseListener);
        }


            // BELOW IS PROBABLY UNNECESSARY
            /*ParsedRequestListener<List<User>> parsedRequestListener = new ParsedRequestListener<List<User>>() {
                @Override
                public void onResponse(List<User> response) {

                    for (User response_user : response) {

                    }

                    userPreferences = changedPreferences(); // It will be new preferences values

                    Toast.makeText(getContext(),

                    //   setPreferencesValues(); // DO WE NEED THEM?
                    //   changeView();

                    if (!isCreatedBefore) {
                        isCreatedBefore = true;
                    }

                }

                @Override
                public void onError(ANError anError) {
                    anError.printStackTrace();
                }
            };*/
            //NetworkHandler.self.getUserProfileImages(parsedRequestListener);


    }


    public void setPreferencesValues() {

        ID = userPreferences.getId();
        userID = userPreferences.getUser().getId();
        preferredGender = userPreferences.getPreferredGender();
        minimumAge = userPreferences.getMinimumAge();
        maximumAge = userPreferences.getMaximumAge();
        maximumDistance = userPreferences.getMaximumDistance();
        minimumRanking = userPreferences.getMinimumRanking();
        maximumRanking = userPreferences.getMaximumRanking();
        discoverable = userPreferences.getDiscoverable();
        universityFilter = userPreferences.getUniversityFilter();
        //rankingFilter = userPreferences.getRankingFilter();

    }


/*
    public void getRealPreferencesValues()
    {
       ID = sharedPreferences.getInt("pref_ID", Personal.MAIN_USER.getId());
       userID = sharedPreferences.getInt("pref_userID", Personal.MAIN_USER.getId());
       preferredGender = sharedPreferences.getInt("pref_gender", Gender.FEMALE);
       minimumAge = sharedPreferences.getInt("pref_minAge", PreferenceChoice.AGE_MIN); // Int to short??
       maximumAge = sharedPreferences.getInt("pref_maxAge", PreferenceChoice.AGE_MAX);
       maximumDistance = sharedPreferences.getInt("pref_maxDist", PreferenceChoice.DISTANCE_MAX);
       minimumRanking = sharedPreferences.getInt("pref_minRank", PreferenceChoice.RANKING_MIN);
       maximumRanking = sharedPreferences.getInt("pref_maxRank", PreferenceChoice.RANKING_MAX);
       discoverable = sharedPreferences.getBoolean("pref_discover", PreferenceChoice.CHECKED);
       universityFilter = sharedPreferences.getBoolean("pref_uniFilter", PreferenceChoice.UNCHECKED);
       rankingFilter = sharedPreferences.getBoolean("pref_rankFilter", PreferenceChoice.UNCHECKED);
    }


    public void commitPreferences()
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("pref_ID", ID);
        editor.putInt("pref_userID", userID);
        editor.putInt("pref_gender", preferredGender);
        editor.putInt("pref_minAge", minimumAge);
        editor.putInt("pref_maxAge", maximumAge);
        editor.putInt("pref_maxDist", maximumDistance);
        editor.putInt("pref_minRank", minimumRanking);
        editor.putInt("pref_maxRank", maximumRanking);
        editor.putBoolean("pref_discover", discoverable);
        editor.putBoolean("pref_uniFilter", universityFilter);
        editor.putBoolean("pref_rankFilter", rankingFilter);
        editor.commit();

    }
*/

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // This will prevent crashes if changes could not applied before changing tab
        AndroidNetworking.forceCancelAll();
    }


}
