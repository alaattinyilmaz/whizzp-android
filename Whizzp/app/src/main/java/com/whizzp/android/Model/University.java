package com.whizzp.android.Model;



//@Entity
public class University {
	//@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	/**
	 * The full name of the university
	 */
	private String name;
	/**
	 * URL of its logo
	 */
	private String logo;
	/**
	 * The number of people in the university
	 */
	private Integer population = 0;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public Integer getPopulation() {
		return population;
	}

	public void setPopulation(Integer population) {
		this.population = population;
	}

}
