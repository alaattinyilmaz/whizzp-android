package com.whizzp.android;


import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.bumptech.glide.Glide;

import okhttp3.Response;
import com.whizzp.android.Constants.Personal;
import com.whizzp.android.Model.User;
import com.whizzp.android.Views.placeholderview.SwipePlaceHolderView;
import com.whizzp.android.Views.placeholderview.annotations.Layout;
import com.whizzp.android.Views.placeholderview.annotations.Resolve;
import com.whizzp.android.Views.placeholderview.annotations.View;
import com.whizzp.android.Views.placeholderview.annotations.swipe.SwipeArchieve;
import com.whizzp.android.Views.placeholderview.annotations.swipe.SwipeArchieveState;
import com.whizzp.android.Views.placeholderview.annotations.swipe.SwipeCancelState;
import com.whizzp.android.Views.placeholderview.annotations.swipe.SwipeIn;
import com.whizzp.android.Views.placeholderview.annotations.swipe.SwipeInState;
import com.whizzp.android.Views.placeholderview.annotations.swipe.SwipeOut;
import com.whizzp.android.Views.placeholderview.annotations.swipe.SwipeOutState;

@Layout(com.whizzp.android.R.layout.rate_cardview)
public class RateCard {

    @View(com.whizzp.android.R.id.rprofileImageView)
    private ImageView profileImageView;

    @View(com.whizzp.android.R.id.rnameAgeTxt)
    private TextView nameAgeTxt;

    @View(com.whizzp.android.R.id.rlocationNameTxt)
    private TextView locationNameTxt;

    @View(com.whizzp.android.R.id.ratingBar)
    private RatingBar ratingBar;

    private User mProfile;
    private Context mContext;
    private SwipePlaceHolderView mSwipeView;
    private RankingFragment rankingFragment;
    private FragmentManager fragmentManager;
    private UserProfileFragment userProfileFragment;

    public static final int RATE_LOW = 0;
    public static final int RATE_AVERAGE = 2;
    public static final int RATE_HIGH = 1;


    public static final int RATE_ONE_STAR = 1;
    public static final int RATE_TWO_STAR = 2;
    public static final int RATE_THREE_STAR = 3;
    public static final int RATE_FOUR_STAR = 4;
    public static final int RATE_FIVE_STAR = 5;


    //  private RatingBar ratingBar;



    public RateCard(Context context, User user, SwipePlaceHolderView swipeView, RankingFragment rankingFragment) {
        mContext = context;
        mProfile = user;
        mSwipeView = swipeView;
        this.rankingFragment = rankingFragment;
        fragmentManager =  Personal.FRAGMENT_MANAGER;
    }



    @Resolve
    private void onResolved() {
        Glide.with(mContext).load(mProfile.getProfilePicture()).into(profileImageView);
        nameAgeTxt.setText(mProfile.getFirstName() + ", " + mProfile.getLastName());
        locationNameTxt.setText(mProfile.getLastName());


        profileImageView.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {

                if(userProfileFragment == null) {
                    // Creating new fragment for each CLICKED user
                    userProfileFragment = new UserProfileFragment();
                }

                // Passing user details to created fragment
                userProfileFragment.passUser(mProfile);

                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(com.whizzp.android.R.id.main_container, userProfileFragment).addToBackStack("UserProfile").commit();


            }
        });

        // TODO: Here will be implementing ranking requests
           ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float float_rating, boolean fromUser) {

                int rating = (int)(float_rating);

                if(rating == RATE_ONE_STAR){
                    // TODO: Here will be json post request
                    mSwipeView.doSwipe(RATE_LOW);
                }

                else if (rating == RATE_TWO_STAR){

                    mSwipeView.doSwipe(RATE_LOW);
                }

                else if (rating == RATE_THREE_STAR){

                    mSwipeView.doSwipe(RATE_AVERAGE);
                }

                else if (rating == RATE_FOUR_STAR){
                    mSwipeView.doSwipe(RATE_HIGH);
                }

                else if (rating == RATE_FIVE_STAR){
                    mSwipeView.doSwipe(RATE_HIGH);
                }
                if(NetworkHandler.self != null){
                    OkHttpResponseListener okHttpResponseListener = new OkHttpResponseListener() {
                        @Override
                        public void onResponse(Response response) {

                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    };
                    NetworkHandler.self.rateUser("" + mProfile.getId(), "" + rating, okHttpResponseListener);
                }

            }
        });

    }


    @SwipeOut
    private void onSwipedOut() {
      //  mSwipeView.addView(this);
        swipeAction();
    }

    @SwipeCancelState
    private void onSwipeCancelState() {
        Log.d("EVENT", "onSwipeCancelState");
    }

    @SwipeIn
    private void onSwipeIn() {
        swipeAction();
    }

    @SwipeArchieve
    private void onSwipeArchieve() {
        swipeAction();
    }


    @SwipeInState
    private void onSwipeInState() {

    }

    @SwipeOutState
    private void onSwipeOutState() {

    }

    @SwipeArchieveState
    private void onSwipeArchieveState() { Log.d("EVENT", "onSwipeArchieveState"); }


    // Since a profile is swipped iff it is the first element in the list, so delete the 0th element.
    private void swipeAction() {
        this.rankingFragment.rateCards.remove(0);
        this.rankingFragment.mUsers.remove(0);
    }
}