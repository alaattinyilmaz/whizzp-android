package com.whizzp.android;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.whizzp.android.Constants.Personal;
import com.whizzp.android.Model.User;
import com.whizzp.android.Views.bottomnavigationbar.BottomNavigationViewEx;

import okhttp3.Response;

// import com.google.android.gms.location.FusedLocationProviderClient;

//import com.google.android.gms.location.FusedLocationProviderClient;

public class MainActivity extends AppCompatActivity {
    private static final int HOME_FRAGMENT = 0;
    private static final int ARCHIVE_FRAGMENT = 1;
    private static final int RANKING_FRAGMENT = 2;
    private static final int LIKE_FRAGMENT = 3;
    private static final int PROFILE_FRAGMENT = 4;
    private static final int SETTINGS_FRAGMENT = 5;
    private static final int CHAT_FRAGMENT = 6;
    private static final int FRAGMENT_COUNT = 7;


    public static MainActivity self;
    public static final int LOCATION_ACCESS_REQ = 1;


    private Fragment[] fragments;
    private Fragment currentFragment;
    UserProfileFragment userProfileFragment;
    private FragmentManager fragmentManager;

    private Toolbar myToolbar;
    private ImageButton settings_view;
    private ImageButton chat_view;
    // private FusedLocationProviderClient mFusedLocationClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.whizzp.android.R.layout.activity_main);

        Personal.CONTEXT_MAIN = this;
        // Asking user to give location permissions
        requireAllPermissions();

        // TODO:  Debug


        NetworkHandler.self = new NetworkHandler(getPreferences(Context.MODE_PRIVATE));
      //  NetworkHandler.self.logout(getPreferences(Context.MODE_PRIVATE));


        NetworkHandler.self.isAuthorized(new OkHttpResponseListener() {
            @Override
            public void onResponse(Response response) {
                if (response.code() == 200) { // HTTP OK
                    onCreateLoader();
                } else {
                    /*Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(loginIntent);
                    finish(); // avoid back button*/
                    System.out.println("Not authorized, please login with facebook");
                    //suppose login with facebook and receive facebook token from system and response from registration server!
                    String username = "emiralaattinyilmaz";//this is the email of facebook account;
                    String facebookToken = "token"; //this is the token received from FACEBOOK!
                    NetworkHandler.self.authorize(username, facebookToken, getPreferences(Context.MODE_PRIVATE), MainActivity.this);
                }
            }

            @Override
            public void onError(ANError anError) {
                System.out.println(anError.getErrorCode());
                // TODO: network err, try again
                Toast.makeText(MainActivity.this, "NETWORK ERR!!", Toast.LENGTH_LONG).show();

            }
        });


        /*
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);


        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            location.getLatitude();
                            location.getAltitude();
                            Toast.makeText(MainActivity.this, String.valueOf(location.getLatitude()+" "+location.getLongitude()), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        */


    }


    public void showBackButton() {
        settings_view.setVisibility(View.INVISIBLE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void hideBackButton() {
        settings_view.setVisibility(View.VISIBLE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    public void clearBackStack() {
        if (fragmentManager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = fragmentManager.getBackStackEntryAt(0);
            fragmentManager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_ACCESS_REQ: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //  startScan();
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Whizzp! Location Acsses needed!");
                    builder.setMessage("We need location access to perform!");
                    builder.setPositiveButton(android.R.string.ok, null);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                //ASK AGAIN
                                requireAllPermissions();
                            }

                        });
                    }
                    builder.show();
                }
                break;
            }
        }
    }


    protected void requireAllPermissions() {
        /* Location acc */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Location Permission");
                builder.setMessage("Whizzp needs your location permission to show you people around you.");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_ACCESS_REQ);
                        }
                    }
                });
                builder.show();
            }
        } else {
        }
    }

    protected void onCreateLoader() {

        // EXPERIMENTAL: For personal profil page
        //Personal.MAIN_USER = new User("Emir Alaattin", "Yılmaz", "https://scontent.fada1-4.fna.fbcdn.net/v/t1.0-9/17796749_1386770284720547_6676867554869722638_n.jpg?oh=fe5aaf977f79cf1502283c3c20b68630&oe=59D51554");
        NetworkHandler.self.getMyself(new ParsedRequestListener<User>() {
            @Override
            public void onResponse(User response) {
                Personal.MAIN_USER = response;
            }

            @Override
            public void onError(ANError anError) {
                System.out.println("error " + anError);
                anError.printStackTrace();
                // TODO raise an error

            }
        });
        fragments = new Fragment[FRAGMENT_COUNT];

        // Creating of toolbar and setting as action bar
        myToolbar = (Toolbar) findViewById(com.whizzp.android.R.id.top_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false); // Hiding the app title
        settings_view = (ImageButton) myToolbar.findViewById(com.whizzp.android.R.id.settings_view);
        chat_view = (ImageButton) myToolbar.findViewById(com.whizzp.android.R.id.chat_view);


        // Bottom navigation bar settings (hiding shifting and disabling animation mode)
        BottomNavigationViewEx bottom_navigation_bar = (BottomNavigationViewEx) findViewById(com.whizzp.android.R.id.bottom_navigation);
        bottom_navigation_bar.enableAnimation(false);
        bottom_navigation_bar.enableShiftingMode(false);
        bottom_navigation_bar.enableItemShiftingMode(false);


        Personal.FRAGMENT_MANAGER = getSupportFragmentManager();
        fragmentManager = Personal.FRAGMENT_MANAGER;


        //initial fragment set in here
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        currentFragment = fragments[HOME_FRAGMENT] = new HomeFragment();
        fragmentTransaction.replace(com.whizzp.android.R.id.main_container, currentFragment, "HOME_FRAGMENT").commit();

        bottom_navigation_bar.setOnNavigationItemSelectedListener(
                new BottomNavigationViewEx.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case com.whizzp.android.R.id.home_view:
                                currentFragment = fragments[HOME_FRAGMENT];
                                break;
                            case com.whizzp.android.R.id.arch_view:
                                if (fragments[ARCHIVE_FRAGMENT] == null) {
                                    fragments[ARCHIVE_FRAGMENT] = new ArchiveFragment();
                                }
                                currentFragment = fragments[ARCHIVE_FRAGMENT];
                                break;
                            case com.whizzp.android.R.id.ranking_view:
                                if (fragments[RANKING_FRAGMENT] == null) {
                                    fragments[RANKING_FRAGMENT] = new RankingFragment();
                                }
                                currentFragment = fragments[RANKING_FRAGMENT];
                                break;
                            case com.whizzp.android.R.id.like_view:
                                if (fragments[LIKE_FRAGMENT] == null) {
                                    fragments[LIKE_FRAGMENT] = new LikeFragment();
                                }
                                currentFragment = fragments[LIKE_FRAGMENT];
                                break;
                            case com.whizzp.android.R.id.profile_view:
                                if (fragments[PROFILE_FRAGMENT] == null) {
                                    userProfileFragment = new UserProfileFragment();
                                    userProfileFragment.passUser(Personal.MAIN_USER);
                                }
                                fragments[PROFILE_FRAGMENT] = userProfileFragment;

                                currentFragment = fragments[PROFILE_FRAGMENT];
                                break;

                        }

                        clearBackStack();
                        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(com.whizzp.android.R.id.main_container, currentFragment).commit();

                        if (settings_view.isSelected()) {
                            settings_view.setSelected(false);
                        } else if (chat_view.isSelected()) {
                            chat_view.setSelected(false);
                        }

                        return true;
                    }
                });


        settings_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settings_view.setSelected(true);
                chat_view.setSelected(false);

                if (fragments[SETTINGS_FRAGMENT] == null) {
                    fragments[SETTINGS_FRAGMENT] = new SettingsFragment();
                }
                currentFragment = fragments[SETTINGS_FRAGMENT];

                clearBackStack();
                final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(com.whizzp.android.R.id.main_container, currentFragment).commit();

            }
        });

        chat_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                chat_view.setSelected(true);
                settings_view.setSelected(false);

                if (fragments[CHAT_FRAGMENT] == null) {
                    fragments[CHAT_FRAGMENT] = new ChatFragment();
                }
                currentFragment = fragments[CHAT_FRAGMENT];

                clearBackStack();
                final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(com.whizzp.android.R.id.main_container, currentFragment).commit();

            }
        });


        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

                if (fragmentManager.getBackStackEntryCount() != 0) {
                    showBackButton();
                } else {
                    hideBackButton();
                }

            }
        });
    }


}
