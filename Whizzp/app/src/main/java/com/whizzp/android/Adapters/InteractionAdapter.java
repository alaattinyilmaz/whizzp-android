package com.whizzp.android.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import com.whizzp.android.Model.User;
import com.whizzp.android.R;

public class InteractionAdapter extends ArrayAdapter<User> {

    public InteractionAdapter(Context context, List<User> items) {
        super(context, 0, items);
    }
    private TextView title;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        User item = getItem(position);

            // Check if an existing view is being reused, otherwise inflate the view
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_like_interaction, parent, false);
            title = (TextView) convertView.findViewById(R.id.like_username_view);

            ImageView caption = (ImageView) convertView.findViewById(R.id.like_profile_picture_view);

        title.setText(item.getFirstName());
        // date.setText(item.getLastname());

        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Selamun Aleykum", Toast.LENGTH_SHORT).show();
            }
        });

        return convertView;
    }


}
