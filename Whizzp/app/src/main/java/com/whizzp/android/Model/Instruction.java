package com.whizzp.android.Model;

public class Instruction {

    private int image_drawable;

    private String instruction_text;

    public String getInstruction_text() {
        return instruction_text;
    }

    public void setInstruction_text(String instruction_text) {
        this.instruction_text = instruction_text;
    }

    public int getImageDrawable() {
        return image_drawable;
    }

    public void setImageDrawable(int image_drawable) {
        this.image_drawable = image_drawable;
    }

}