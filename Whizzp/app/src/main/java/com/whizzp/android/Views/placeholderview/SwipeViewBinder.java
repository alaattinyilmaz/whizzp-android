package com.whizzp.android.Views.placeholderview;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.graphics.PointF;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.whizzp.android.Views.placeholderview.annotations.swipe.SwipeArchieve;
import com.whizzp.android.Views.placeholderview.annotations.swipe.SwipeArchieveState;
import com.whizzp.android.Views.placeholderview.annotations.swipe.SwipeCancelState;
import com.whizzp.android.Views.placeholderview.annotations.swipe.SwipeIn;
import com.whizzp.android.Views.placeholderview.annotations.swipe.SwipeInState;
import com.whizzp.android.Views.placeholderview.annotations.swipe.SwipeOut;
import com.whizzp.android.Views.placeholderview.annotations.swipe.SwipeOutState;
import com.whizzp.android.Views.placeholderview.annotations.swipe.SwipeView;

import static android.view.View.VISIBLE;

/**
 * Created by janisharali on 26/08/16.
 */
public class SwipeViewBinder<T, V extends FrameLayout> extends ViewBinder<T, V> {

    private V mLayoutView;
    private SwipeCallback mCallback;
    private Animator.AnimatorListener mViewRemoveAnimatorListener;
    private Animator.AnimatorListener mViewRestoreAnimatorListener;
    private Animator.AnimatorListener mViewPutBackAnimatorListener;
    private int mSwipeType = SwipePlaceHolderView.SWIPE_TYPE_DEFAULT;
    private View mSwipeInMsgView;
    private View mSwipeOutMsgView;
    private View mSwipeArchieveMsgView; // Archieve Implementation
    private SwipeDecor mSwipeDecor;
    private SwipePlaceHolderView.SwipeOption mSwipeOption;

    //    TODO: Make mHasInterceptedEvent a AtomicBoolean, to make it thread safe.
    private boolean mHasInterceptedEvent = false;
    private int mOriginalLeftMargin;
    private int mOriginalTopMargin;
    private float mTransXToRestore;
    private float mTransYToRestore;


    // Archive mode implementation
    private int mSwipeChoice;
    public static final int SWIPE_DISLIKE = 0;
    public static final int SWIPE_LIKE = 1;
    public static final int SWIPE_ARCHIEVE = 2;

    // Ranking mode implementation
    private int mStarNumber;
    public static final int ONE_STAR = 1;
    public static final int TWO_STAR = 2;
    public static final int THREE_STAR = 3;
    public static final int FOUR_STAR = 4;
    public static final int FIVE_STAR = 5;




    /**
     *
     * @param resolver
     */
    public SwipeViewBinder(T resolver) {
        super(resolver);
    }

    /**
     *
     * @param promptsView
     * @param position
     * @param swipeType
     * @param decor
     * @param callback
     */
    protected void bindView(V promptsView, int position, int swipeType, SwipeDecor decor,
                            SwipePlaceHolderView.SwipeOption swipeOption, SwipeCallback callback) {
        mLayoutView = promptsView;
        mSwipeType = swipeType;
        mSwipeOption = swipeOption;
        mSwipeDecor = decor;
        mCallback = callback;

        bindSwipeView(promptsView);
        bindViews(getResolver(), promptsView);
        bindViewPosition(getResolver(), position);
        resolveView(getResolver());
    }

    /**
     *
     */
    protected void setOnTouch(){
        bindClick(getResolver(), getLayoutView());
        bindLongPress(getResolver(), getLayoutView());
        switch (mSwipeType){
            case SwipePlaceHolderView.SWIPE_TYPE_DEFAULT:
                setDefaultTouchListener(mLayoutView);
                break;
            case SwipePlaceHolderView.SWIPE_TYPE_HORIZONTAL:
                setHorizontalTouchListener(mLayoutView);
                break;
            case SwipePlaceHolderView.SWIPE_TYPE_VERTICAL:
                setVerticalTouchListener(mLayoutView);
                break;
        }
    }

    private void bindSwipeView(V promptsView){
        T resolver = getResolver();
        for(final Field field : resolver.getClass().getDeclaredFields()) {
            SwipeView annotation = field.getAnnotation(SwipeView.class);
            if(annotation != null) {
                try {
                    field.setAccessible(true);
                    field.set(resolver, promptsView);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     *
     * @param resolver
     */
    private void bindSwipeIn(final T resolver){
        for(final Method method : resolver.getClass().getDeclaredMethods()) {
            SwipeIn annotation = method.getAnnotation(SwipeIn.class);
            if(annotation != null) {
                try {
                    method.setAccessible(true);
                    method.invoke(resolver);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }




    /**
     *
     * @param resolver
     */
    private void bindSwipeOut(final T resolver){
        for(final Method method : resolver.getClass().getDeclaredMethods()) {
            SwipeOut annotation = method.getAnnotation(SwipeOut.class);
            if(annotation != null) {
                try {
                    method.setAccessible(true);
                    method.invoke(resolver);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     *
     * @param resolver
     */
    private void bindSwipeArchieve(final T resolver){
        for(final Method method : resolver.getClass().getDeclaredMethods()) {
            SwipeArchieve annotation = method.getAnnotation(SwipeArchieve.class);
            if(annotation != null) {
                try {
                    method.setAccessible(true);
                    method.invoke(resolver);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }




    /**
     *
     */
    protected void bindSwipeInState(){
        for(final Method method : getResolver().getClass().getDeclaredMethods()) {
            SwipeInState annotation = method.getAnnotation(SwipeInState.class);
            if(annotation != null) {
                try {
                    method.setAccessible(true);
                    method.invoke(getResolver());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     *
     */
    protected void bindSwipeOutState(){
        for(final Method method : getResolver().getClass().getDeclaredMethods()) {
            SwipeOutState annotation = method.getAnnotation(SwipeOutState.class);
            if(annotation != null) {
                try {
                    method.setAccessible(true);
                    method.invoke(getResolver());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }



    /**
     *
     */
    protected void bindSwipeArchieveState(){
        for(final Method method : getResolver().getClass().getDeclaredMethods()) {
            SwipeArchieveState annotation = method.getAnnotation(SwipeArchieveState.class);
            if(annotation != null) {
                try {
                    method.setAccessible(true);
                    method.invoke(getResolver());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /**
     *
     */
    protected void bindSwipeCancelState(){
        for(final Method method : getResolver().getClass().getDeclaredMethods()) {
            SwipeCancelState annotation = method.getAnnotation(SwipeCancelState.class);
            if(annotation != null) {
                try {
                    method.setAccessible(true);
                    method.invoke(getResolver());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     *
     */
    @Override
    protected void unbind() {
        super.unbind();
    }

    /**
     *
     */
    private void serAnimatorListener(){
        mViewRemoveAnimatorListener = new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {}

            @Override
            public void onAnimationEnd(Animator animation) {
                if(mSwipeOption.getIsPutBackActive()){
                    mLayoutView.animate()
                            .translationX(mTransXToRestore)
                            .translationY(mTransYToRestore)
                            .setInterpolator(new AccelerateInterpolator(mSwipeDecor.getSwipeAnimFactor()))
                            .setDuration((long)(mSwipeDecor.getSwipeAnimTime()))
                            .setListener(mViewPutBackAnimatorListener)
                            .start();
                }else if(mCallback != null){
                    mCallback.onRemoveView(SwipeViewBinder.this);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {}

            @Override
            public void onAnimationRepeat(Animator animation) {}
        };

        mViewRestoreAnimatorListener = new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {}

            @Override
            public void onAnimationEnd(Animator animation) {
                if(mCallback != null){
                    mCallback.onResetView(SwipeViewBinder.this);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {}

            @Override
            public void onAnimationRepeat(Animator animation) {}
        };

        mViewPutBackAnimatorListener = new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {}

            @Override
            public void onAnimationEnd(Animator animation) {
                animateSwipeRestore(mLayoutView, mOriginalTopMargin, mOriginalLeftMargin, mSwipeType);
            }

            @Override
            public void onAnimationCancel(Animator animation) {}

            @Override
            public void onAnimationRepeat(Animator animation) {}
        };
    }

    /**
     *
     * @param view
     */
    private void setDefaultTouchListener(final V view){
        serAnimatorListener();
        final DisplayMetrics displayMetrics = view.getContext().getResources().getDisplayMetrics();

        FrameLayout.LayoutParams layoutParamsOriginal = (FrameLayout.LayoutParams) view.getLayoutParams();
        mOriginalLeftMargin = layoutParamsOriginal.leftMargin;
        mOriginalTopMargin = layoutParamsOriginal.topMargin;

        mTransXToRestore = view.getTranslationX();
        mTransYToRestore = view.getTranslationY();

        view.setOnTouchListener(new View.OnTouchListener() {
            private float dx;
            private float dy;
            private int activePointerId = SwipeDecor.PRIMITIVE_NULL;
            private boolean resetDone = false;
            private PointF pointerCurrentPoint = new PointF();
            private PointF pointerStartingPoint = new PointF();
            @Override
            public boolean onTouch(final View v, MotionEvent event) {

                if(mSwipeOption.getIsTouchSwipeLocked()){
                    if(mSwipeOption.getIsViewToRestoreOnTouchLock()){
                        mSwipeOption.setIsViewToRestoreOnTouchLock(true);
                        animateSwipeRestore(v, mOriginalTopMargin, mOriginalLeftMargin, mSwipeType);
                    }
                    return true;
                }


                if(mSwipeOption.getIsTouchSwipeLocked()){
                    if(mSwipeOption.getIsViewToRestoreOnTouchLock()){
                        mSwipeOption.setIsViewToRestoreOnTouchLock(false);
                        animateSwipeRestore(v, mOriginalTopMargin, mOriginalLeftMargin, mSwipeType);
                    }
                    return true;
                }


                if(mSwipeOption.getIsViewLocked()){
                    if(mSwipeOption.getIsViewToRestoredOnLock()){
                        mSwipeOption.setIsViewToRestoredOnLock(false);
                        animateSwipeRestore(v, mOriginalTopMargin, mOriginalLeftMargin, mSwipeType);
                    }
                    return true;
                }

                if(!mHasInterceptedEvent){
                    pointerStartingPoint.set(event.getRawX(), event.getRawY());
                    pointerCurrentPoint.set(event.getRawX(), event.getRawY());
                    activePointerId = event.getPointerId(0);
                    resetDone = false;
                    FrameLayout.LayoutParams layoutParamsOriginal = (FrameLayout.LayoutParams) v.getLayoutParams();
                    dx = pointerCurrentPoint.x - layoutParamsOriginal.leftMargin;
                    dy = pointerCurrentPoint.y - layoutParamsOriginal.topMargin;
                    mHasInterceptedEvent = true;
                }

                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        if (event.getPointerId(event.getActionIndex()) == activePointerId && !resetDone) {}
                        else{break;}
                    case MotionEvent.ACTION_UP:

                        if(!resetDone) {
                            float distSlideX = pointerCurrentPoint.x - dx;
                            float distSlideY = pointerCurrentPoint.y - dy;

                            distSlideX = distSlideX < 0 ? -distSlideX : distSlideX;
                            distSlideY = distSlideY < 0 ? -distSlideY : distSlideY;
                            if (distSlideX < displayMetrics.widthPixels / mSwipeOption.getWidthSwipeDistFactor()
                                    && distSlideY < displayMetrics.heightPixels / mSwipeOption.getHeightSwipeDistFactor()) {
                                animateSwipeRestore(v, mOriginalTopMargin, mOriginalLeftMargin, mSwipeType);
                            }
                            else {
                                if(!mSwipeOption.getIsPutBackActive()) {
                                    blockTouch();
                                }

                                float transX = displayMetrics.widthPixels;
                                float transY = displayMetrics.heightPixels;


                                if (pointerCurrentPoint.x >= pointerStartingPoint.x
                                        && pointerCurrentPoint.y >= pointerStartingPoint.y) {
                                    bindSwipeIn(getResolver());
                                } else if (pointerCurrentPoint.x > pointerStartingPoint.x
                                        && pointerCurrentPoint.y < pointerStartingPoint.y) {
                                    transY = -v.getHeight();
                                    bindSwipeIn(getResolver());
                                } else if (pointerCurrentPoint.x < pointerStartingPoint.x
                                        && pointerCurrentPoint.y >= pointerStartingPoint.y) {
                                    transX = -v.getWidth();
                                    bindSwipeOut(getResolver());
                                } else if (pointerCurrentPoint.x <= pointerStartingPoint.x
                                        && pointerCurrentPoint.y < pointerStartingPoint.y) {
                                    transY = -v.getHeight();
                                    transX = -v.getWidth();
                                    bindSwipeOut(getResolver());
                                }


                                view.animate()
                                        .translationX(transX)
                                        .translationY(transY)
                                        .setInterpolator(new AccelerateInterpolator(mSwipeDecor.getSwipeAnimFactor()))
                                        .setDuration((long)(mSwipeDecor.getSwipeAnimTime() * 1.25))
                                        .setListener(mViewRemoveAnimatorListener)
                                        .start();

                            }
                            new CountDownTimer(mSwipeDecor.getSwipeAnimTime(), mSwipeDecor.getSwipeAnimTime()) {
                                public void onTick(long millisUntilFinished) {}
                                public void onFinish() {mHasInterceptedEvent = false;}
                            }.start();
                            resetDone = true;
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if(!resetDone && event.findPointerIndex(activePointerId) != SwipeDecor.PRIMITIVE_NULL) {
                            pointerCurrentPoint.set(event.getRawX(), event.getRawY());
                            FrameLayout.LayoutParams layoutParamsTemp = (FrameLayout.LayoutParams) v.getLayoutParams();
                            layoutParamsTemp.topMargin = (int) (pointerCurrentPoint.y - dy);
                            layoutParamsTemp.leftMargin = (int) (pointerCurrentPoint.x - dx);
                            v.setLayoutParams(layoutParamsTemp);

                            int distanceMovedTop = layoutParamsTemp.topMargin - mOriginalTopMargin;
                            int distanceMovedLeft = layoutParamsTemp.leftMargin - mOriginalLeftMargin;
                            mCallback.onAnimateView(distanceMovedLeft, distanceMovedTop, displayMetrics.widthPixels / mSwipeOption.getWidthSwipeDistFactor(),
                                    displayMetrics.heightPixels / mSwipeOption.getHeightSwipeDistFactor(), SwipeViewBinder.this);
                        }
                        break;
                }
                return true;
            }
        });
    }

    /**
     *
     * @param view
     */
    private void setHorizontalTouchListener(final V view){
        serAnimatorListener();
        final DisplayMetrics displayMetrics = view.getContext().getResources().getDisplayMetrics();

        FrameLayout.LayoutParams layoutParamsOriginal = (FrameLayout.LayoutParams) view.getLayoutParams();
        mOriginalLeftMargin = layoutParamsOriginal.leftMargin;
        mOriginalTopMargin = layoutParamsOriginal.topMargin;
        mTransXToRestore = view.getTranslationX();
        mTransYToRestore = view.getTranslationY();

        view.setOnTouchListener(new View.OnTouchListener() {
            private float xStart;
            private float x;
            private float dx;
            private int activePointerId = SwipeDecor.PRIMITIVE_NULL;
            private boolean resetDone = false;
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(mSwipeOption.getIsTouchSwipeLocked()){
                    if(mSwipeOption.getIsViewToRestoreOnTouchLock()){
                        mSwipeOption.setIsViewToRestoreOnTouchLock(false);
                        animateSwipeRestore(v, mOriginalTopMargin, mOriginalLeftMargin, mSwipeType);
                    }
                    return true;
                }

                if(mSwipeOption.getIsViewLocked()){
                    if(mSwipeOption.getIsViewToRestoredOnLock()){
                        mSwipeOption.setIsViewToRestoredOnLock(false);
                        animateSwipeRestore(v, mOriginalTopMargin, mOriginalLeftMargin, mSwipeType);
                    }
                    return true;
                }

                if(!mHasInterceptedEvent){
                    xStart = event.getRawX();
                    x = event.getRawX();
                    activePointerId = event.getPointerId(0);
                    resetDone = false;
                    FrameLayout.LayoutParams layoutParamsCurrent = (FrameLayout.LayoutParams) v.getLayoutParams();
                    dx = x - layoutParamsCurrent.leftMargin;
                    mHasInterceptedEvent = true;
                }

                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        if (event.getPointerId(event.getActionIndex()) == activePointerId && !resetDone) {}
                        else{break;}
                    case MotionEvent.ACTION_UP:
                        if(!resetDone) {
                            float distSlideX = x - dx;
                            distSlideX = distSlideX < 0 ? -distSlideX : distSlideX;
                            if (distSlideX < displayMetrics.widthPixels / mSwipeOption.getWidthSwipeDistFactor()) {
                                animateSwipeRestore(v, mOriginalTopMargin, mOriginalLeftMargin, mSwipeType);
                            } else {
                                if(!mSwipeOption.getIsPutBackActive()) {
                                    blockTouch();
                                }

                                float transX = displayMetrics.widthPixels;
                                if (x < xStart) {
                                    transX = -v.getWidth();
                                    bindSwipeOut(getResolver());
                                } else {
                                    bindSwipeIn(getResolver());
                                }

                                view.animate()
                                        .translationX(transX)
                                        .setInterpolator(new AccelerateInterpolator(mSwipeDecor.getSwipeAnimFactor()))
                                        .setDuration((long)(mSwipeDecor.getSwipeAnimTime() * 1.25))
                                        .setListener(mViewRemoveAnimatorListener)
                                        .start();
                            }
                            new CountDownTimer(mSwipeDecor.getSwipeAnimTime(), mSwipeDecor.getSwipeAnimTime()) {
                                public void onTick(long millisUntilFinished) {}
                                public void onFinish() {mHasInterceptedEvent = false;}
                            }.start();
                            resetDone = true;
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if(!resetDone && event.findPointerIndex(activePointerId) != SwipeDecor.PRIMITIVE_NULL) {
                            x = event.getRawX();
                            FrameLayout.LayoutParams layoutParamsTemp = (FrameLayout.LayoutParams) v.getLayoutParams();
                            layoutParamsTemp.leftMargin = (int) (x - dx);
                            v.setLayoutParams(layoutParamsTemp);
                            int distanceMoved = layoutParamsTemp.leftMargin - mOriginalLeftMargin;
                            mCallback.onAnimateView(distanceMoved, 0, displayMetrics.widthPixels / mSwipeOption.getWidthSwipeDistFactor(),
                                    displayMetrics.heightPixels / mSwipeOption.getHeightSwipeDistFactor(), SwipeViewBinder.this);
                        }
                        break;
                }
                return true;
            }
        });
    }

    /**
     *
     * @param view
     */
    private void setVerticalTouchListener(final V view){
        serAnimatorListener();
        final DisplayMetrics displayMetrics = view.getContext().getResources().getDisplayMetrics();

        FrameLayout.LayoutParams layoutParamsOriginal = (FrameLayout.LayoutParams) view.getLayoutParams();
        mOriginalLeftMargin = layoutParamsOriginal.leftMargin;
        mOriginalTopMargin = layoutParamsOriginal.topMargin;
        mTransXToRestore = view.getTranslationX();
        mTransYToRestore = view.getTranslationY();

        view.setOnTouchListener(new View.OnTouchListener() {
            private float yStart;
            private float y;
            private float dy;
            private int activePointerId = SwipeDecor.PRIMITIVE_NULL;
            private boolean resetDone = false;
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(mSwipeOption.getIsTouchSwipeLocked()){
                    if(mSwipeOption.getIsViewToRestoreOnTouchLock()){
                        mSwipeOption.setIsViewToRestoreOnTouchLock(false);
                        animateSwipeRestore(v, mOriginalTopMargin, mOriginalLeftMargin, mSwipeType);
                    }
                    return true;
                }

                if(mSwipeOption.getIsViewLocked()){
                    if(mSwipeOption.getIsViewToRestoredOnLock()){
                        mSwipeOption.setIsViewToRestoredOnLock(false);
                        animateSwipeRestore(v, mOriginalTopMargin, mOriginalLeftMargin, mSwipeType);
                    }
                    return true;
                }

                if(!mHasInterceptedEvent){
                    yStart = event.getRawY();
                    y = event.getRawY();
                    activePointerId = event.getPointerId(0);
                    resetDone = false;
                    FrameLayout.LayoutParams layoutParamsOriginal = (FrameLayout.LayoutParams) v.getLayoutParams();
                    dy = y - layoutParamsOriginal.topMargin;
                    mHasInterceptedEvent = true;
                }

                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        if (event.getPointerId(event.getActionIndex()) == activePointerId && !resetDone) {}
                        else{break;}
                    case MotionEvent.ACTION_UP:
                        if(!resetDone) {
                            float distSlideY = y - dy;
                            distSlideY = distSlideY < 0 ? -distSlideY : distSlideY;
                            if (distSlideY < displayMetrics.heightPixels / mSwipeOption.getHeightSwipeDistFactor()) {
                                animateSwipeRestore(v, mOriginalTopMargin, mOriginalLeftMargin, mSwipeType);
                            } else {
                                if(!mSwipeOption.getIsPutBackActive()) {
                                    blockTouch();
                                }

                                float transY = displayMetrics.heightPixels;
                                if (y < yStart) {
                                    transY = -v.getHeight();
                                    bindSwipeOut(getResolver());
                                } else {
                                    bindSwipeIn(getResolver());
                                }
                                view.animate()
                                        .translationY(transY)
                                        .setInterpolator(new AccelerateInterpolator(mSwipeDecor.getSwipeAnimFactor()))
                                        .setDuration((long)(mSwipeDecor.getSwipeAnimTime() * 1.25))
                                        .setListener(mViewRemoveAnimatorListener)
                                        .start();

                            }
                            new CountDownTimer(mSwipeDecor.getSwipeAnimTime(), mSwipeDecor.getSwipeAnimTime()) {
                                public void onTick(long millisUntilFinished) {}
                                public void onFinish() {mHasInterceptedEvent = false;}
                            }.start();
                            resetDone = true;
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if(!resetDone && event.findPointerIndex(activePointerId) != SwipeDecor.PRIMITIVE_NULL) {
                            y = event.getRawY();
                            FrameLayout.LayoutParams layoutParamsTemp = (FrameLayout.LayoutParams) v.getLayoutParams();
                            layoutParamsTemp.topMargin = (int) (y - dy);
                            v.setLayoutParams(layoutParamsTemp);

                            int distanceMoved = layoutParamsTemp.topMargin - mOriginalTopMargin;
                            mCallback.onAnimateView(0, distanceMoved, displayMetrics.widthPixels / mSwipeOption.getWidthSwipeDistFactor(),
                                    displayMetrics.heightPixels / mSwipeOption.getHeightSwipeDistFactor(), SwipeViewBinder.this);
                        }
                        break;
                }
                return true;
            }
        });
    }

    protected void blockTouch(){
        mLayoutView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent rawEvent) {
                return true;
            }
        });
    }

    /**
     *
     * @param v
     * @param originalTopMargin
     * @param originalLeftMargin
     * @param swipeType
     */
    private void animateSwipeRestore(final View v, int originalTopMargin, int originalLeftMargin, int swipeType){
        final FrameLayout.LayoutParams layoutParamsFinal = (FrameLayout.LayoutParams) v.getLayoutParams();

        ValueAnimator animatorX = null;
        ValueAnimator animatorY = null;
        int animTime = mSwipeDecor.getSwipeAnimTime();
        DecelerateInterpolator decelerateInterpolator = new DecelerateInterpolator(mSwipeDecor.getSwipeAnimFactor());
        ViewPropertyAnimator animatorR = v.animate()
                .rotation(0)
                .setInterpolator(decelerateInterpolator)
                .setDuration(animTime)
                .setListener(mViewRestoreAnimatorListener);

        if(swipeType == SwipePlaceHolderView.SWIPE_TYPE_DEFAULT
                || swipeType == SwipePlaceHolderView.SWIPE_TYPE_HORIZONTAL){
            animatorX = ValueAnimator.ofInt(layoutParamsFinal.leftMargin, originalLeftMargin);
            animatorX.setInterpolator(decelerateInterpolator);
            animatorX.setDuration(animTime);
            animatorX.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    layoutParamsFinal.leftMargin = (Integer) valueAnimator.getAnimatedValue();
                    v.setLayoutParams(layoutParamsFinal);
                }

            });
        }
        if(swipeType == SwipePlaceHolderView.SWIPE_TYPE_DEFAULT
                || swipeType == SwipePlaceHolderView.SWIPE_TYPE_VERTICAL){
            animatorY = ValueAnimator.ofInt(layoutParamsFinal.topMargin, originalTopMargin);
            animatorY.setInterpolator(decelerateInterpolator);
            animatorY.setDuration(animTime);
            animatorY.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    layoutParamsFinal.topMargin = (Integer) valueAnimator.getAnimatedValue();
                    v.setLayoutParams(layoutParamsFinal);
                }
            });
        }

        if(animatorX != null){
            animatorX.start();
        }
        if(animatorY != null){
            animatorY.start();
        }
        animatorR.start();
    }

    /**
     *
     * @param mSwipeChoice
     */
    protected void doSwipe(int mSwipeChoice){
        if(mLayoutView != null && mViewRemoveAnimatorListener != null && !mSwipeOption.getIsViewLocked()) {
            if(!mSwipeOption.getIsPutBackActive()) {
                blockTouch();
            }

            if (mSwipeChoice == SWIPE_LIKE) {
                if (getSwipeInMsgView() != null) {
                   getSwipeInMsgView().setVisibility(VISIBLE);
                }
            }

            else if(mSwipeChoice == SWIPE_DISLIKE)
            {
                if (getSwipeOutMsgView() != null) {
                    getSwipeOutMsgView().setVisibility(VISIBLE);
                }
            }

            else if(mSwipeChoice == SWIPE_ARCHIEVE){
                if (getSwipeArchieveMsgView() != null) {
                    getSwipeArchieveMsgView().setVisibility(VISIBLE);
                }
            }

            DisplayMetrics displayMetrics = mLayoutView.getResources().getDisplayMetrics();
            ViewPropertyAnimator animator = mLayoutView.animate();

            float transX = displayMetrics.widthPixels;
            float transY = displayMetrics.heightPixels;
            switch (mSwipeType){
                case SwipePlaceHolderView.SWIPE_TYPE_DEFAULT:
                    if((mSwipeChoice == SWIPE_LIKE)){
                        bindSwipeIn(getResolver());
                        animator.rotation(-mSwipeDecor.getSwipeRotationAngle());
                    }

                    else if (mSwipeChoice == SWIPE_DISLIKE)
                    {
                        bindSwipeOut(getResolver());
                        transX = -mLayoutView.getWidth();
                        animator.rotation(mSwipeDecor.getSwipeRotationAngle());
                    }

                    else if (mSwipeChoice == SWIPE_ARCHIEVE)
                    {
                        bindSwipeArchieve(getResolver());
                        transX = 0; // To make it move to below
                        // TODO: 30.06.2017 : ARCHIVE WILL BE IMPLEMENTED
                        animator.rotation(-mSwipeDecor.getSwipeRotationAngle());
                    }
                    animator.translationX(transX).translationY(transY);
                    break;


                case SwipePlaceHolderView.SWIPE_TYPE_HORIZONTAL:
                    if(mSwipeChoice == SWIPE_LIKE){
                        bindSwipeIn(getResolver());
                    }
                    else if (mSwipeChoice == SWIPE_DISLIKE)
                    {
                        bindSwipeOut(getResolver());
                        transX = -mLayoutView.getWidth();
                    }

                    else if (mSwipeChoice == SWIPE_ARCHIEVE)
                    {
                        bindSwipeArchieve(getResolver());
                        transX = 0;
                        // TODO: 30.06.2017 : ARCHIVE WILL BE IMPLEMENTED
                    }
                    animator.translationX(transX);
                    break;
                case SwipePlaceHolderView.SWIPE_TYPE_VERTICAL:
                    if(mSwipeChoice == SWIPE_LIKE){
                        bindSwipeIn(getResolver());
                    }


                    else if (mSwipeChoice == SWIPE_DISLIKE)
                    {
                        bindSwipeOut(getResolver());
                        transY = -mLayoutView.getHeight();
                    }

                    else if (mSwipeChoice == SWIPE_ARCHIEVE)
                    {
                        bindSwipeArchieve(getResolver());
                        transY = 0;
                        // TODO: 30.06.2017 : ARCHIVE WILL BE IMPLEMENTED
                    }

                    animator.translationY(transY);
                    break;
            }

            animator.setDuration((long) (mSwipeDecor.getSwipeAnimTime() * 1.25))
                    .setInterpolator(new AccelerateInterpolator(mSwipeDecor.getSwipeAnimFactor()))
                    .setListener(mViewRemoveAnimatorListener)
                    .start();
        }
    }




    /**
     *
     * @param mStarNumber
     */
    protected void doRating(int mStarNumber){
        if(mLayoutView != null && mViewRemoveAnimatorListener != null && !mSwipeOption.getIsViewLocked()) {
            if(!mSwipeOption.getIsPutBackActive()) {
                blockTouch();
            }

            if (mStarNumber == ONE_STAR) {
                if (getSwipeInMsgView() != null) {
                    getSwipeInMsgView().setVisibility(VISIBLE);
                }
            }

            else if(mStarNumber == TWO_STAR)
            {
                if (getSwipeOutMsgView() != null) {
                    getSwipeOutMsgView().setVisibility(VISIBLE);
                }
            }

            else if(mStarNumber == THREE_STAR){
                if (getSwipeArchieveMsgView() != null) {
                    getSwipeArchieveMsgView().setVisibility(VISIBLE);
                }
            }

            else if(mStarNumber == FOUR_STAR){
                if (getSwipeArchieveMsgView() != null) {
                    getSwipeArchieveMsgView().setVisibility(VISIBLE);
                }
            }

            else if(mStarNumber == FIVE_STAR){
                if (getSwipeArchieveMsgView() != null) {
                    getSwipeArchieveMsgView().setVisibility(VISIBLE);
                }
            }

            DisplayMetrics displayMetrics = mLayoutView.getResources().getDisplayMetrics();
            ViewPropertyAnimator animator = mLayoutView.animate();

            float transX = displayMetrics.widthPixels;
            float transY = displayMetrics.heightPixels;
            switch (mSwipeType){
                case SwipePlaceHolderView.SWIPE_TYPE_DEFAULT:
                    if((mStarNumber == ONE_STAR)){
                        bindSwipeIn(getResolver());
                        animator.rotation(-mSwipeDecor.getSwipeRotationAngle());
                    }

                    else if (mStarNumber == TWO_STAR)
                    {
                        bindSwipeOut(getResolver());
                        transX = -mLayoutView.getWidth();
                        animator.rotation(mSwipeDecor.getSwipeRotationAngle());
                    }


                    else if (mStarNumber == THREE_STAR)
                    {
                        bindSwipeOut(getResolver());
                        transX = -mLayoutView.getWidth();
                        animator.rotation(mSwipeDecor.getSwipeRotationAngle());
                    }

                    else if (mStarNumber == FOUR_STAR)
                    {
                        bindSwipeOut(getResolver());
                        transX = -mLayoutView.getWidth();
                        animator.rotation(mSwipeDecor.getSwipeRotationAngle());
                    }

                    else if (mStarNumber == FIVE_STAR)
                    {
                        bindSwipeArchieve(getResolver());
                        transX = 0; // To make it move to below
                        // TODO: 30.06.2017 : ARCHIVE WILL BE IMPLEMENTED
                        animator.rotation(-mSwipeDecor.getSwipeRotationAngle());
                    }
                    animator.translationX(transX).translationY(transY);
                    break;


                case SwipePlaceHolderView.SWIPE_TYPE_HORIZONTAL:
                    if(mStarNumber == ONE_STAR){
                        bindSwipeIn(getResolver());
                    }
                    else if (mStarNumber == TWO_STAR)
                    {
                        bindSwipeOut(getResolver());
                        transX = -mLayoutView.getWidth();
                    }

                    else if (mStarNumber == THREE_STAR)
                    {
                        bindSwipeOut(getResolver());
                        transX = -mLayoutView.getWidth();
                    }

                    else if (mStarNumber == FOUR_STAR)
                    {
                        bindSwipeOut(getResolver());
                        transX = -mLayoutView.getWidth();
                    }

                    else if (mStarNumber == FIVE_STAR)
                    {
                        bindSwipeArchieve(getResolver());
                        transX = 0;
                        // TODO: 30.06.2017 : ARCHIVE WILL BE IMPLEMENTED
                    }
                    animator.translationX(transX);
                    break;
                case SwipePlaceHolderView.SWIPE_TYPE_VERTICAL:
                    if(mStarNumber == ONE_STAR){
                        bindSwipeIn(getResolver());
                    }


                    else if (mSwipeChoice == TWO_STAR)
                    {
                        bindSwipeOut(getResolver());
                        transY = -mLayoutView.getHeight();
                    }

                    else if (mSwipeChoice == THREE_STAR)
                    {
                        bindSwipeArchieve(getResolver());
                        transY = 0;
                        // TODO: 30.06.2017 : ARCHIVE WILL BE IMPLEMENTED
                    }


                    else if (mSwipeChoice == FOUR_STAR)
                    {
                        bindSwipeArchieve(getResolver());
                        transY = 0;
                        // TODO: 30.06.2017 : ARCHIVE WILL BE IMPLEMENTED
                    }


                    else if (mSwipeChoice == FIVE_STAR)
                    {
                        bindSwipeArchieve(getResolver());
                        transY = 0;
                        // TODO: 30.06.2017 : ARCHIVE WILL BE IMPLEMENTED
                    }

                    animator.translationY(transY);
                    break;
            }

            animator.setDuration((long) (mSwipeDecor.getSwipeAnimTime() * 1.25))
                    .setInterpolator(new AccelerateInterpolator(mSwipeDecor.getSwipeAnimFactor()))
                    .setListener(mViewRemoveAnimatorListener)
                    .start();
        }
    }





    /**
     *
     * @return
     */
    protected View getSwipeInMsgView() {
        return mSwipeInMsgView;
    }

    /**
     *
     * @param swipeInMsgView
     */
    protected void setSwipeInMsgView(View swipeInMsgView) {
        mSwipeInMsgView = swipeInMsgView;
    }

    /**
     *
     * @return
     */
    protected View getSwipeOutMsgView() {
        return mSwipeOutMsgView;
    }

    /**
     *
     * @return
     */
    protected View getSwipeArchieveMsgView() {
        return mSwipeArchieveMsgView;
    }


    /**
     *
     * @param swipeOutMsgView
     */
    protected void setSwipeOutMsgView(View swipeOutMsgView) {
        mSwipeOutMsgView = swipeOutMsgView;
    }



    /**
     *
     * @param swipeArchieveMsgView
     */
    protected void setSwipeArchieveMsgView(View swipeArchieveMsgView) {
        mSwipeArchieveMsgView = swipeArchieveMsgView;
    }


    /**
     *
     * @return
     */
    protected V getLayoutView() {
        return mLayoutView;
    }

    /**
     *
     * @param <T>
     */
    protected interface SwipeCallback<T extends SwipeViewBinder<?, ? extends FrameLayout>>{
        void onRemoveView(T swipeViewBinder);
        void onResetView(T swipeViewBinder);
        void onAnimateView(float distXMoved, float distYMoved, float finalXDist, float finalYDist, T swipeViewBinder);
    }
}
